/**
 */
package mdsebook.simplegraph;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mdsebook.simplegraph.Node#getOutgoingEdges <em>Outgoing Edges</em>}</li>
 * </ul>
 *
 * @see mdsebook.simplegraph.SimplegraphPackage#getNode()
 * @model
 * @generated
 */
public interface Node extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Outgoing Edges</b></em>' containment reference list.
	 * The list contents are of type {@link mdsebook.simplegraph.Edge}.
	 * It is bidirectional and its opposite is '{@link mdsebook.simplegraph.Edge#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outgoing Edges</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outgoing Edges</em>' containment reference list.
	 * @see mdsebook.simplegraph.SimplegraphPackage#getNode_OutgoingEdges()
	 * @see mdsebook.simplegraph.Edge#getSource
	 * @model opposite="source" containment="true"
	 * @generated
	 */
	EList<Edge> getOutgoingEdges();

} // Node
