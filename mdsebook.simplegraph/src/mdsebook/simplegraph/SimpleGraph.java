/**
 */
package mdsebook.simplegraph;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simple Graph</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mdsebook.simplegraph.SimpleGraph#getNode <em>Node</em>}</li>
 * </ul>
 *
 * @see mdsebook.simplegraph.SimplegraphPackage#getSimpleGraph()
 * @model
 * @generated
 */
public interface SimpleGraph extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Node</b></em>' containment reference list.
	 * The list contents are of type {@link mdsebook.simplegraph.Node}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Node</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Node</em>' containment reference list.
	 * @see mdsebook.simplegraph.SimplegraphPackage#getSimpleGraph_Node()
	 * @model containment="true"
	 * @generated
	 */
	EList<Node> getNode();

} // SimpleGraph
