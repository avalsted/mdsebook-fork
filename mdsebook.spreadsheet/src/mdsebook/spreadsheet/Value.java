/**
 */
package mdsebook.spreadsheet;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Value</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see mdsebook.spreadsheet.SpreadsheetPackage#getValue()
 * @model abstract="true"
 * @generated
 */
public interface Value extends EObject {
} // Value
