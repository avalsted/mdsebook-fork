/**
 */
package mdsebook.spreadsheet;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Formula</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see mdsebook.spreadsheet.SpreadsheetPackage#getFormula()
 * @model abstract="true"
 * @generated
 */
public interface Formula extends Value {
} // Formula
