/**
 */
package mdsebook.spreadsheet;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mdsebook.spreadsheet.Reference#getPosition <em>Position</em>}</li>
 * </ul>
 *
 * @see mdsebook.spreadsheet.SpreadsheetPackage#getReference()
 * @model
 * @generated
 */
public interface Reference extends Formula {
	/**
	 * Returns the value of the '<em><b>Position</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Position</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Position</em>' reference.
	 * @see #setPosition(Cell)
	 * @see mdsebook.spreadsheet.SpreadsheetPackage#getReference_Position()
	 * @model required="true"
	 * @generated
	 */
	Cell getPosition();

	/**
	 * Sets the value of the '{@link mdsebook.spreadsheet.Reference#getPosition <em>Position</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Position</em>' reference.
	 * @see #getPosition()
	 * @generated
	 */
	void setPosition(Cell value);

} // Reference
