/**
 */
package mdsebook.spreadsheet;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bin Exp</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mdsebook.spreadsheet.BinExp#getLeft <em>Left</em>}</li>
 *   <li>{@link mdsebook.spreadsheet.BinExp#getRight <em>Right</em>}</li>
 *   <li>{@link mdsebook.spreadsheet.BinExp#getOp <em>Op</em>}</li>
 * </ul>
 *
 * @see mdsebook.spreadsheet.SpreadsheetPackage#getBinExp()
 * @model
 * @generated
 */
public interface BinExp extends Formula {
	/**
	 * Returns the value of the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Left</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left</em>' containment reference.
	 * @see #setLeft(Formula)
	 * @see mdsebook.spreadsheet.SpreadsheetPackage#getBinExp_Left()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Formula getLeft();

	/**
	 * Sets the value of the '{@link mdsebook.spreadsheet.BinExp#getLeft <em>Left</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Left</em>' containment reference.
	 * @see #getLeft()
	 * @generated
	 */
	void setLeft(Formula value);

	/**
	 * Returns the value of the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Right</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right</em>' containment reference.
	 * @see #setRight(Formula)
	 * @see mdsebook.spreadsheet.SpreadsheetPackage#getBinExp_Right()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Formula getRight();

	/**
	 * Sets the value of the '{@link mdsebook.spreadsheet.BinExp#getRight <em>Right</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right</em>' containment reference.
	 * @see #getRight()
	 * @generated
	 */
	void setRight(Formula value);

	/**
	 * Returns the value of the '<em><b>Op</b></em>' attribute.
	 * The literals are from the enumeration {@link mdsebook.spreadsheet.Operators}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Op</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Op</em>' attribute.
	 * @see mdsebook.spreadsheet.Operators
	 * @see #setOp(Operators)
	 * @see mdsebook.spreadsheet.SpreadsheetPackage#getBinExp_Op()
	 * @model
	 * @generated
	 */
	Operators getOp();

	/**
	 * Sets the value of the '{@link mdsebook.spreadsheet.BinExp#getOp <em>Op</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Op</em>' attribute.
	 * @see mdsebook.spreadsheet.Operators
	 * @see #getOp()
	 * @generated
	 */
	void setOp(Operators value);

} // BinExp
