/**
 */
package mdsebook.spreadsheet;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see mdsebook.spreadsheet.SpreadsheetFactory
 * @model kind="package"
 * @generated
 */
public interface SpreadsheetPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "spreadsheet";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.mdsebook.org/mdsebook.spreadsheet";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "mdsebook.spreadsheet";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SpreadsheetPackage eINSTANCE = mdsebook.spreadsheet.impl.SpreadsheetPackageImpl.init();

	/**
	 * The meta object id for the '{@link mdsebook.spreadsheet.impl.ValueImpl <em>Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mdsebook.spreadsheet.impl.ValueImpl
	 * @see mdsebook.spreadsheet.impl.SpreadsheetPackageImpl#getValue()
	 * @generated
	 */
	int VALUE = 0;

	/**
	 * The number of structural features of the '<em>Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link mdsebook.spreadsheet.impl.NamedElementImpl <em>Named Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mdsebook.spreadsheet.impl.NamedElementImpl
	 * @see mdsebook.spreadsheet.impl.SpreadsheetPackageImpl#getNamedElement()
	 * @generated
	 */
	int NAMED_ELEMENT = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__NAME = 0;

	/**
	 * The number of structural features of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link mdsebook.spreadsheet.impl.PositionImpl <em>Position</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mdsebook.spreadsheet.impl.PositionImpl
	 * @see mdsebook.spreadsheet.impl.SpreadsheetPackageImpl#getPosition()
	 * @generated
	 */
	int POSITION = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POSITION__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>Position</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POSITION_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Column</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POSITION___COLUMN = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Row</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POSITION___ROW = NAMED_ELEMENT_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>Position</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POSITION_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link mdsebook.spreadsheet.impl.SheetImpl <em>Sheet</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mdsebook.spreadsheet.impl.SheetImpl
	 * @see mdsebook.spreadsheet.impl.SpreadsheetPackageImpl#getSheet()
	 * @generated
	 */
	int SHEET = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHEET__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Cell</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHEET__CELL = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Sheet</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHEET_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Sheet</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHEET_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link mdsebook.spreadsheet.impl.CellImpl <em>Cell</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mdsebook.spreadsheet.impl.CellImpl
	 * @see mdsebook.spreadsheet.impl.SpreadsheetPackageImpl#getCell()
	 * @generated
	 */
	int CELL = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CELL__NAME = POSITION__NAME;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CELL__VALUE = POSITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Cell</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CELL_FEATURE_COUNT = POSITION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Column</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CELL___COLUMN = POSITION___COLUMN;

	/**
	 * The operation id for the '<em>Row</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CELL___ROW = POSITION___ROW;

	/**
	 * The operation id for the '<em>Left</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CELL___LEFT = POSITION_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Right</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CELL___RIGHT = POSITION_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Up</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CELL___UP = POSITION_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Down</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CELL___DOWN = POSITION_OPERATION_COUNT + 3;

	/**
	 * The number of operations of the '<em>Cell</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CELL_OPERATION_COUNT = POSITION_OPERATION_COUNT + 4;

	/**
	 * The meta object id for the '{@link mdsebook.spreadsheet.impl.SimpleImpl <em>Simple</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mdsebook.spreadsheet.impl.SimpleImpl
	 * @see mdsebook.spreadsheet.impl.SpreadsheetPackageImpl#getSimple()
	 * @generated
	 */
	int SIMPLE = 5;

	/**
	 * The feature id for the '<em><b>Contents</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE__CONTENTS = VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Simple</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_FEATURE_COUNT = VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Simple</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_OPERATION_COUNT = VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link mdsebook.spreadsheet.impl.FormulaImpl <em>Formula</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mdsebook.spreadsheet.impl.FormulaImpl
	 * @see mdsebook.spreadsheet.impl.SpreadsheetPackageImpl#getFormula()
	 * @generated
	 */
	int FORMULA = 6;

	/**
	 * The number of structural features of the '<em>Formula</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMULA_FEATURE_COUNT = VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Formula</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMULA_OPERATION_COUNT = VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link mdsebook.spreadsheet.impl.ConstExpImpl <em>Const Exp</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mdsebook.spreadsheet.impl.ConstExpImpl
	 * @see mdsebook.spreadsheet.impl.SpreadsheetPackageImpl#getConstExp()
	 * @generated
	 */
	int CONST_EXP = 7;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONST_EXP__VALUE = FORMULA_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Const Exp</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONST_EXP_FEATURE_COUNT = FORMULA_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Const Exp</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONST_EXP_OPERATION_COUNT = FORMULA_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link mdsebook.spreadsheet.impl.ReferenceImpl <em>Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mdsebook.spreadsheet.impl.ReferenceImpl
	 * @see mdsebook.spreadsheet.impl.SpreadsheetPackageImpl#getReference()
	 * @generated
	 */
	int REFERENCE = 8;

	/**
	 * The feature id for the '<em><b>Position</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE__POSITION = FORMULA_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_FEATURE_COUNT = FORMULA_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_OPERATION_COUNT = FORMULA_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link mdsebook.spreadsheet.impl.RangeSumImpl <em>Range Sum</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mdsebook.spreadsheet.impl.RangeSumImpl
	 * @see mdsebook.spreadsheet.impl.SpreadsheetPackageImpl#getRangeSum()
	 * @generated
	 */
	int RANGE_SUM = 9;

	/**
	 * The feature id for the '<em><b>Top Left</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_SUM__TOP_LEFT = FORMULA_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Bottom Right</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_SUM__BOTTOM_RIGHT = FORMULA_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Range Sum</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_SUM_FEATURE_COUNT = FORMULA_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Range Sum</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_SUM_OPERATION_COUNT = FORMULA_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link mdsebook.spreadsheet.impl.BinExpImpl <em>Bin Exp</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mdsebook.spreadsheet.impl.BinExpImpl
	 * @see mdsebook.spreadsheet.impl.SpreadsheetPackageImpl#getBinExp()
	 * @generated
	 */
	int BIN_EXP = 10;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIN_EXP__LEFT = FORMULA_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIN_EXP__RIGHT = FORMULA_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Op</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIN_EXP__OP = FORMULA_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Bin Exp</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIN_EXP_FEATURE_COUNT = FORMULA_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Bin Exp</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIN_EXP_OPERATION_COUNT = FORMULA_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link mdsebook.spreadsheet.Operators <em>Operators</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mdsebook.spreadsheet.Operators
	 * @see mdsebook.spreadsheet.impl.SpreadsheetPackageImpl#getOperators()
	 * @generated
	 */
	int OPERATORS = 11;


	/**
	 * Returns the meta object for class '{@link mdsebook.spreadsheet.Value <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Value</em>'.
	 * @see mdsebook.spreadsheet.Value
	 * @generated
	 */
	EClass getValue();

	/**
	 * Returns the meta object for class '{@link mdsebook.spreadsheet.Position <em>Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Position</em>'.
	 * @see mdsebook.spreadsheet.Position
	 * @generated
	 */
	EClass getPosition();

	/**
	 * Returns the meta object for the '{@link mdsebook.spreadsheet.Position#column() <em>Column</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Column</em>' operation.
	 * @see mdsebook.spreadsheet.Position#column()
	 * @generated
	 */
	EOperation getPosition__Column();

	/**
	 * Returns the meta object for the '{@link mdsebook.spreadsheet.Position#row() <em>Row</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Row</em>' operation.
	 * @see mdsebook.spreadsheet.Position#row()
	 * @generated
	 */
	EOperation getPosition__Row();

	/**
	 * Returns the meta object for class '{@link mdsebook.spreadsheet.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Element</em>'.
	 * @see mdsebook.spreadsheet.NamedElement
	 * @generated
	 */
	EClass getNamedElement();

	/**
	 * Returns the meta object for the attribute '{@link mdsebook.spreadsheet.NamedElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see mdsebook.spreadsheet.NamedElement#getName()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Name();

	/**
	 * Returns the meta object for class '{@link mdsebook.spreadsheet.Sheet <em>Sheet</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sheet</em>'.
	 * @see mdsebook.spreadsheet.Sheet
	 * @generated
	 */
	EClass getSheet();

	/**
	 * Returns the meta object for the containment reference list '{@link mdsebook.spreadsheet.Sheet#getCell <em>Cell</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Cell</em>'.
	 * @see mdsebook.spreadsheet.Sheet#getCell()
	 * @see #getSheet()
	 * @generated
	 */
	EReference getSheet_Cell();

	/**
	 * Returns the meta object for class '{@link mdsebook.spreadsheet.Cell <em>Cell</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cell</em>'.
	 * @see mdsebook.spreadsheet.Cell
	 * @generated
	 */
	EClass getCell();

	/**
	 * Returns the meta object for the containment reference '{@link mdsebook.spreadsheet.Cell#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see mdsebook.spreadsheet.Cell#getValue()
	 * @see #getCell()
	 * @generated
	 */
	EReference getCell_Value();

	/**
	 * Returns the meta object for the '{@link mdsebook.spreadsheet.Cell#left() <em>Left</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Left</em>' operation.
	 * @see mdsebook.spreadsheet.Cell#left()
	 * @generated
	 */
	EOperation getCell__Left();

	/**
	 * Returns the meta object for the '{@link mdsebook.spreadsheet.Cell#right() <em>Right</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Right</em>' operation.
	 * @see mdsebook.spreadsheet.Cell#right()
	 * @generated
	 */
	EOperation getCell__Right();

	/**
	 * Returns the meta object for the '{@link mdsebook.spreadsheet.Cell#up() <em>Up</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Up</em>' operation.
	 * @see mdsebook.spreadsheet.Cell#up()
	 * @generated
	 */
	EOperation getCell__Up();

	/**
	 * Returns the meta object for the '{@link mdsebook.spreadsheet.Cell#down() <em>Down</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Down</em>' operation.
	 * @see mdsebook.spreadsheet.Cell#down()
	 * @generated
	 */
	EOperation getCell__Down();

	/**
	 * Returns the meta object for class '{@link mdsebook.spreadsheet.Simple <em>Simple</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simple</em>'.
	 * @see mdsebook.spreadsheet.Simple
	 * @generated
	 */
	EClass getSimple();

	/**
	 * Returns the meta object for the attribute '{@link mdsebook.spreadsheet.Simple#getContents <em>Contents</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Contents</em>'.
	 * @see mdsebook.spreadsheet.Simple#getContents()
	 * @see #getSimple()
	 * @generated
	 */
	EAttribute getSimple_Contents();

	/**
	 * Returns the meta object for class '{@link mdsebook.spreadsheet.Formula <em>Formula</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Formula</em>'.
	 * @see mdsebook.spreadsheet.Formula
	 * @generated
	 */
	EClass getFormula();

	/**
	 * Returns the meta object for class '{@link mdsebook.spreadsheet.ConstExp <em>Const Exp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Const Exp</em>'.
	 * @see mdsebook.spreadsheet.ConstExp
	 * @generated
	 */
	EClass getConstExp();

	/**
	 * Returns the meta object for the attribute '{@link mdsebook.spreadsheet.ConstExp#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see mdsebook.spreadsheet.ConstExp#getValue()
	 * @see #getConstExp()
	 * @generated
	 */
	EAttribute getConstExp_Value();

	/**
	 * Returns the meta object for class '{@link mdsebook.spreadsheet.Reference <em>Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reference</em>'.
	 * @see mdsebook.spreadsheet.Reference
	 * @generated
	 */
	EClass getReference();

	/**
	 * Returns the meta object for the reference '{@link mdsebook.spreadsheet.Reference#getPosition <em>Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Position</em>'.
	 * @see mdsebook.spreadsheet.Reference#getPosition()
	 * @see #getReference()
	 * @generated
	 */
	EReference getReference_Position();

	/**
	 * Returns the meta object for class '{@link mdsebook.spreadsheet.RangeSum <em>Range Sum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Range Sum</em>'.
	 * @see mdsebook.spreadsheet.RangeSum
	 * @generated
	 */
	EClass getRangeSum();

	/**
	 * Returns the meta object for the reference '{@link mdsebook.spreadsheet.RangeSum#getTopLeft <em>Top Left</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Top Left</em>'.
	 * @see mdsebook.spreadsheet.RangeSum#getTopLeft()
	 * @see #getRangeSum()
	 * @generated
	 */
	EReference getRangeSum_TopLeft();

	/**
	 * Returns the meta object for the reference '{@link mdsebook.spreadsheet.RangeSum#getBottomRight <em>Bottom Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Bottom Right</em>'.
	 * @see mdsebook.spreadsheet.RangeSum#getBottomRight()
	 * @see #getRangeSum()
	 * @generated
	 */
	EReference getRangeSum_BottomRight();

	/**
	 * Returns the meta object for class '{@link mdsebook.spreadsheet.BinExp <em>Bin Exp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Bin Exp</em>'.
	 * @see mdsebook.spreadsheet.BinExp
	 * @generated
	 */
	EClass getBinExp();

	/**
	 * Returns the meta object for the containment reference '{@link mdsebook.spreadsheet.BinExp#getLeft <em>Left</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left</em>'.
	 * @see mdsebook.spreadsheet.BinExp#getLeft()
	 * @see #getBinExp()
	 * @generated
	 */
	EReference getBinExp_Left();

	/**
	 * Returns the meta object for the containment reference '{@link mdsebook.spreadsheet.BinExp#getRight <em>Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right</em>'.
	 * @see mdsebook.spreadsheet.BinExp#getRight()
	 * @see #getBinExp()
	 * @generated
	 */
	EReference getBinExp_Right();

	/**
	 * Returns the meta object for the attribute '{@link mdsebook.spreadsheet.BinExp#getOp <em>Op</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Op</em>'.
	 * @see mdsebook.spreadsheet.BinExp#getOp()
	 * @see #getBinExp()
	 * @generated
	 */
	EAttribute getBinExp_Op();

	/**
	 * Returns the meta object for enum '{@link mdsebook.spreadsheet.Operators <em>Operators</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Operators</em>'.
	 * @see mdsebook.spreadsheet.Operators
	 * @generated
	 */
	EEnum getOperators();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	SpreadsheetFactory getSpreadsheetFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link mdsebook.spreadsheet.impl.ValueImpl <em>Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mdsebook.spreadsheet.impl.ValueImpl
		 * @see mdsebook.spreadsheet.impl.SpreadsheetPackageImpl#getValue()
		 * @generated
		 */
		EClass VALUE = eINSTANCE.getValue();

		/**
		 * The meta object literal for the '{@link mdsebook.spreadsheet.impl.PositionImpl <em>Position</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mdsebook.spreadsheet.impl.PositionImpl
		 * @see mdsebook.spreadsheet.impl.SpreadsheetPackageImpl#getPosition()
		 * @generated
		 */
		EClass POSITION = eINSTANCE.getPosition();

		/**
		 * The meta object literal for the '<em><b>Column</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation POSITION___COLUMN = eINSTANCE.getPosition__Column();

		/**
		 * The meta object literal for the '<em><b>Row</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation POSITION___ROW = eINSTANCE.getPosition__Row();

		/**
		 * The meta object literal for the '{@link mdsebook.spreadsheet.impl.NamedElementImpl <em>Named Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mdsebook.spreadsheet.impl.NamedElementImpl
		 * @see mdsebook.spreadsheet.impl.SpreadsheetPackageImpl#getNamedElement()
		 * @generated
		 */
		EClass NAMED_ELEMENT = eINSTANCE.getNamedElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ELEMENT__NAME = eINSTANCE.getNamedElement_Name();

		/**
		 * The meta object literal for the '{@link mdsebook.spreadsheet.impl.SheetImpl <em>Sheet</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mdsebook.spreadsheet.impl.SheetImpl
		 * @see mdsebook.spreadsheet.impl.SpreadsheetPackageImpl#getSheet()
		 * @generated
		 */
		EClass SHEET = eINSTANCE.getSheet();

		/**
		 * The meta object literal for the '<em><b>Cell</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SHEET__CELL = eINSTANCE.getSheet_Cell();

		/**
		 * The meta object literal for the '{@link mdsebook.spreadsheet.impl.CellImpl <em>Cell</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mdsebook.spreadsheet.impl.CellImpl
		 * @see mdsebook.spreadsheet.impl.SpreadsheetPackageImpl#getCell()
		 * @generated
		 */
		EClass CELL = eINSTANCE.getCell();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CELL__VALUE = eINSTANCE.getCell_Value();

		/**
		 * The meta object literal for the '<em><b>Left</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CELL___LEFT = eINSTANCE.getCell__Left();

		/**
		 * The meta object literal for the '<em><b>Right</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CELL___RIGHT = eINSTANCE.getCell__Right();

		/**
		 * The meta object literal for the '<em><b>Up</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CELL___UP = eINSTANCE.getCell__Up();

		/**
		 * The meta object literal for the '<em><b>Down</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CELL___DOWN = eINSTANCE.getCell__Down();

		/**
		 * The meta object literal for the '{@link mdsebook.spreadsheet.impl.SimpleImpl <em>Simple</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mdsebook.spreadsheet.impl.SimpleImpl
		 * @see mdsebook.spreadsheet.impl.SpreadsheetPackageImpl#getSimple()
		 * @generated
		 */
		EClass SIMPLE = eINSTANCE.getSimple();

		/**
		 * The meta object literal for the '<em><b>Contents</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMPLE__CONTENTS = eINSTANCE.getSimple_Contents();

		/**
		 * The meta object literal for the '{@link mdsebook.spreadsheet.impl.FormulaImpl <em>Formula</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mdsebook.spreadsheet.impl.FormulaImpl
		 * @see mdsebook.spreadsheet.impl.SpreadsheetPackageImpl#getFormula()
		 * @generated
		 */
		EClass FORMULA = eINSTANCE.getFormula();

		/**
		 * The meta object literal for the '{@link mdsebook.spreadsheet.impl.ConstExpImpl <em>Const Exp</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mdsebook.spreadsheet.impl.ConstExpImpl
		 * @see mdsebook.spreadsheet.impl.SpreadsheetPackageImpl#getConstExp()
		 * @generated
		 */
		EClass CONST_EXP = eINSTANCE.getConstExp();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONST_EXP__VALUE = eINSTANCE.getConstExp_Value();

		/**
		 * The meta object literal for the '{@link mdsebook.spreadsheet.impl.ReferenceImpl <em>Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mdsebook.spreadsheet.impl.ReferenceImpl
		 * @see mdsebook.spreadsheet.impl.SpreadsheetPackageImpl#getReference()
		 * @generated
		 */
		EClass REFERENCE = eINSTANCE.getReference();

		/**
		 * The meta object literal for the '<em><b>Position</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENCE__POSITION = eINSTANCE.getReference_Position();

		/**
		 * The meta object literal for the '{@link mdsebook.spreadsheet.impl.RangeSumImpl <em>Range Sum</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mdsebook.spreadsheet.impl.RangeSumImpl
		 * @see mdsebook.spreadsheet.impl.SpreadsheetPackageImpl#getRangeSum()
		 * @generated
		 */
		EClass RANGE_SUM = eINSTANCE.getRangeSum();

		/**
		 * The meta object literal for the '<em><b>Top Left</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RANGE_SUM__TOP_LEFT = eINSTANCE.getRangeSum_TopLeft();

		/**
		 * The meta object literal for the '<em><b>Bottom Right</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RANGE_SUM__BOTTOM_RIGHT = eINSTANCE.getRangeSum_BottomRight();

		/**
		 * The meta object literal for the '{@link mdsebook.spreadsheet.impl.BinExpImpl <em>Bin Exp</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mdsebook.spreadsheet.impl.BinExpImpl
		 * @see mdsebook.spreadsheet.impl.SpreadsheetPackageImpl#getBinExp()
		 * @generated
		 */
		EClass BIN_EXP = eINSTANCE.getBinExp();

		/**
		 * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BIN_EXP__LEFT = eINSTANCE.getBinExp_Left();

		/**
		 * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BIN_EXP__RIGHT = eINSTANCE.getBinExp_Right();

		/**
		 * The meta object literal for the '<em><b>Op</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BIN_EXP__OP = eINSTANCE.getBinExp_Op();

		/**
		 * The meta object literal for the '{@link mdsebook.spreadsheet.Operators <em>Operators</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mdsebook.spreadsheet.Operators
		 * @see mdsebook.spreadsheet.impl.SpreadsheetPackageImpl#getOperators()
		 * @generated
		 */
		EEnum OPERATORS = eINSTANCE.getOperators();

	}

} //SpreadsheetPackage
