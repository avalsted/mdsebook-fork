/**
 */
package mdsebook.spreadsheet.impl;

import mdsebook.spreadsheet.Formula;
import mdsebook.spreadsheet.SpreadsheetPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Formula</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class FormulaImpl extends ValueImpl implements Formula {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FormulaImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SpreadsheetPackage.Literals.FORMULA;
	}

} //FormulaImpl
