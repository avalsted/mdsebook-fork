/**
 */
package mdsebook.spreadsheet.impl;

import mdsebook.spreadsheet.Cell;
import mdsebook.spreadsheet.RangeSum;
import mdsebook.spreadsheet.SpreadsheetPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Range Sum</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mdsebook.spreadsheet.impl.RangeSumImpl#getTopLeft <em>Top Left</em>}</li>
 *   <li>{@link mdsebook.spreadsheet.impl.RangeSumImpl#getBottomRight <em>Bottom Right</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RangeSumImpl extends FormulaImpl implements RangeSum {
	/**
	 * The cached value of the '{@link #getTopLeft() <em>Top Left</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTopLeft()
	 * @generated
	 * @ordered
	 */
	protected Cell topLeft;

	/**
	 * The cached value of the '{@link #getBottomRight() <em>Bottom Right</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBottomRight()
	 * @generated
	 * @ordered
	 */
	protected Cell bottomRight;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RangeSumImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SpreadsheetPackage.Literals.RANGE_SUM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Cell getTopLeft() {
		if (topLeft != null && topLeft.eIsProxy()) {
			InternalEObject oldTopLeft = (InternalEObject)topLeft;
			topLeft = (Cell)eResolveProxy(oldTopLeft);
			if (topLeft != oldTopLeft) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SpreadsheetPackage.RANGE_SUM__TOP_LEFT, oldTopLeft, topLeft));
			}
		}
		return topLeft;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Cell basicGetTopLeft() {
		return topLeft;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTopLeft(Cell newTopLeft) {
		Cell oldTopLeft = topLeft;
		topLeft = newTopLeft;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpreadsheetPackage.RANGE_SUM__TOP_LEFT, oldTopLeft, topLeft));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Cell getBottomRight() {
		if (bottomRight != null && bottomRight.eIsProxy()) {
			InternalEObject oldBottomRight = (InternalEObject)bottomRight;
			bottomRight = (Cell)eResolveProxy(oldBottomRight);
			if (bottomRight != oldBottomRight) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SpreadsheetPackage.RANGE_SUM__BOTTOM_RIGHT, oldBottomRight, bottomRight));
			}
		}
		return bottomRight;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Cell basicGetBottomRight() {
		return bottomRight;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBottomRight(Cell newBottomRight) {
		Cell oldBottomRight = bottomRight;
		bottomRight = newBottomRight;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpreadsheetPackage.RANGE_SUM__BOTTOM_RIGHT, oldBottomRight, bottomRight));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SpreadsheetPackage.RANGE_SUM__TOP_LEFT:
				if (resolve) return getTopLeft();
				return basicGetTopLeft();
			case SpreadsheetPackage.RANGE_SUM__BOTTOM_RIGHT:
				if (resolve) return getBottomRight();
				return basicGetBottomRight();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SpreadsheetPackage.RANGE_SUM__TOP_LEFT:
				setTopLeft((Cell)newValue);
				return;
			case SpreadsheetPackage.RANGE_SUM__BOTTOM_RIGHT:
				setBottomRight((Cell)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SpreadsheetPackage.RANGE_SUM__TOP_LEFT:
				setTopLeft((Cell)null);
				return;
			case SpreadsheetPackage.RANGE_SUM__BOTTOM_RIGHT:
				setBottomRight((Cell)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SpreadsheetPackage.RANGE_SUM__TOP_LEFT:
				return topLeft != null;
			case SpreadsheetPackage.RANGE_SUM__BOTTOM_RIGHT:
				return bottomRight != null;
		}
		return super.eIsSet(featureID);
	}

} //RangeSumImpl
