/**
 */
package mdsebook.spreadsheet;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Exp</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see mdsebook.spreadsheet.SpreadsheetPackage#getExp()
 * @model abstract="true"
 * @generated
 */
public interface Exp extends Value {
} // Exp
