/**
 */
package mdsebook.spreadsheet;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Range Sum</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mdsebook.spreadsheet.RangeSum#getTopLeft <em>Top Left</em>}</li>
 *   <li>{@link mdsebook.spreadsheet.RangeSum#getBottomRight <em>Bottom Right</em>}</li>
 * </ul>
 *
 * @see mdsebook.spreadsheet.SpreadsheetPackage#getRangeSum()
 * @model
 * @generated
 */
public interface RangeSum extends Formula {
	/**
	 * Returns the value of the '<em><b>Top Left</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Top Left</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Top Left</em>' reference.
	 * @see #setTopLeft(Cell)
	 * @see mdsebook.spreadsheet.SpreadsheetPackage#getRangeSum_TopLeft()
	 * @model required="true"
	 * @generated
	 */
	Cell getTopLeft();

	/**
	 * Sets the value of the '{@link mdsebook.spreadsheet.RangeSum#getTopLeft <em>Top Left</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Top Left</em>' reference.
	 * @see #getTopLeft()
	 * @generated
	 */
	void setTopLeft(Cell value);

	/**
	 * Returns the value of the '<em><b>Bottom Right</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bottom Right</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bottom Right</em>' reference.
	 * @see #setBottomRight(Cell)
	 * @see mdsebook.spreadsheet.SpreadsheetPackage#getRangeSum_BottomRight()
	 * @model required="true"
	 * @generated
	 */
	Cell getBottomRight();

	/**
	 * Sets the value of the '{@link mdsebook.spreadsheet.RangeSum#getBottomRight <em>Bottom Right</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bottom Right</em>' reference.
	 * @see #getBottomRight()
	 * @generated
	 */
	void setBottomRight(Cell value);

} // RangeSum
