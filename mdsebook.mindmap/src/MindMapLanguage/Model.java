/**
 */
package MindMapLanguage;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link MindMapLanguage.Model#getEditorVersion <em>Editor Version</em>}</li>
 *   <li>{@link MindMapLanguage.Model#getMindmap <em>Mindmap</em>}</li>
 *   <li>{@link MindMapLanguage.Model#getColors <em>Colors</em>}</li>
 * </ul>
 *
 * @see MindMapLanguage.MindMapLanguagePackage#getModel()
 * @model
 * @generated
 */
public interface Model extends EObject {
	/**
	 * Returns the value of the '<em><b>Editor Version</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Editor Version</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Editor Version</em>' attribute.
	 * @see #setEditorVersion(int)
	 * @see MindMapLanguage.MindMapLanguagePackage#getModel_EditorVersion()
	 * @model default="1"
	 * @generated
	 */
	int getEditorVersion();

	/**
	 * Sets the value of the '{@link MindMapLanguage.Model#getEditorVersion <em>Editor Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Editor Version</em>' attribute.
	 * @see #getEditorVersion()
	 * @generated
	 */
	void setEditorVersion(int value);

	/**
	 * Returns the value of the '<em><b>Mindmap</b></em>' containment reference list.
	 * The list contents are of type {@link MindMapLanguage.MindMap}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mindmap</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mindmap</em>' containment reference list.
	 * @see MindMapLanguage.MindMapLanguagePackage#getModel_Mindmap()
	 * @model containment="true"
	 * @generated
	 */
	EList<MindMap> getMindmap();

	/**
	 * Returns the value of the '<em><b>Colors</b></em>' containment reference list.
	 * The list contents are of type {@link MindMapLanguage.Color}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Colors</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Colors</em>' containment reference list.
	 * @see MindMapLanguage.MindMapLanguagePackage#getModel_Colors()
	 * @model containment="true"
	 * @generated
	 */
	EList<Color> getColors();

} // Model
