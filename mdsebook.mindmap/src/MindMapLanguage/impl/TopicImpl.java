/**
 */
package MindMapLanguage.impl;

import MindMapLanguage.Color;
import MindMapLanguage.MindMapLanguagePackage;
import MindMapLanguage.Topic;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Topic</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link MindMapLanguage.impl.TopicImpl#getSubtopics <em>Subtopics</em>}</li>
 *   <li>{@link MindMapLanguage.impl.TopicImpl#getOrder <em>Order</em>}</li>
 *   <li>{@link MindMapLanguage.impl.TopicImpl#isEmphasized <em>Emphasized</em>}</li>
 *   <li>{@link MindMapLanguage.impl.TopicImpl#getColor <em>Color</em>}</li>
 *   <li>{@link MindMapLanguage.impl.TopicImpl#getRelatedTo <em>Related To</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TopicImpl extends NamedElementImpl implements Topic {
	/**
	 * The cached value of the '{@link #getSubtopics() <em>Subtopics</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubtopics()
	 * @generated
	 * @ordered
	 */
	protected EList<Topic> subtopics;

	/**
	 * The default value of the '{@link #getOrder() <em>Order</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrder()
	 * @generated
	 * @ordered
	 */
	protected static final int ORDER_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getOrder() <em>Order</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrder()
	 * @generated
	 * @ordered
	 */
	protected int order = ORDER_EDEFAULT;

	/**
	 * The default value of the '{@link #isEmphasized() <em>Emphasized</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isEmphasized()
	 * @generated
	 * @ordered
	 */
	protected static final boolean EMPHASIZED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isEmphasized() <em>Emphasized</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isEmphasized()
	 * @generated
	 * @ordered
	 */
	protected boolean emphasized = EMPHASIZED_EDEFAULT;

	/**
	 * The cached value of the '{@link #getColor() <em>Color</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColor()
	 * @generated
	 * @ordered
	 */
	protected Color color;

	/**
	 * The cached value of the '{@link #getRelatedTo() <em>Related To</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelatedTo()
	 * @generated
	 * @ordered
	 */
	protected EList<Topic> relatedTo;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TopicImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MindMapLanguagePackage.Literals.TOPIC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Topic> getSubtopics() {
		if (subtopics == null) {
			subtopics = new EObjectContainmentEList<Topic>(Topic.class, this, MindMapLanguagePackage.TOPIC__SUBTOPICS);
		}
		return subtopics;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getOrder() {
		return order;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrder(int newOrder) {
		int oldOrder = order;
		order = newOrder;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MindMapLanguagePackage.TOPIC__ORDER, oldOrder, order));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isEmphasized() {
		return emphasized;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEmphasized(boolean newEmphasized) {
		boolean oldEmphasized = emphasized;
		emphasized = newEmphasized;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MindMapLanguagePackage.TOPIC__EMPHASIZED, oldEmphasized, emphasized));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Color getColor() {
		if (color != null && color.eIsProxy()) {
			InternalEObject oldColor = (InternalEObject)color;
			color = (Color)eResolveProxy(oldColor);
			if (color != oldColor) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MindMapLanguagePackage.TOPIC__COLOR, oldColor, color));
			}
		}
		return color;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Color basicGetColor() {
		return color;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setColor(Color newColor) {
		Color oldColor = color;
		color = newColor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MindMapLanguagePackage.TOPIC__COLOR, oldColor, color));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Topic> getRelatedTo() {
		if (relatedTo == null) {
			relatedTo = new EObjectResolvingEList<Topic>(Topic.class, this, MindMapLanguagePackage.TOPIC__RELATED_TO);
		}
		return relatedTo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MindMapLanguagePackage.TOPIC__SUBTOPICS:
				return ((InternalEList<?>)getSubtopics()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MindMapLanguagePackage.TOPIC__SUBTOPICS:
				return getSubtopics();
			case MindMapLanguagePackage.TOPIC__ORDER:
				return getOrder();
			case MindMapLanguagePackage.TOPIC__EMPHASIZED:
				return isEmphasized();
			case MindMapLanguagePackage.TOPIC__COLOR:
				if (resolve) return getColor();
				return basicGetColor();
			case MindMapLanguagePackage.TOPIC__RELATED_TO:
				return getRelatedTo();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MindMapLanguagePackage.TOPIC__SUBTOPICS:
				getSubtopics().clear();
				getSubtopics().addAll((Collection<? extends Topic>)newValue);
				return;
			case MindMapLanguagePackage.TOPIC__ORDER:
				setOrder((Integer)newValue);
				return;
			case MindMapLanguagePackage.TOPIC__EMPHASIZED:
				setEmphasized((Boolean)newValue);
				return;
			case MindMapLanguagePackage.TOPIC__COLOR:
				setColor((Color)newValue);
				return;
			case MindMapLanguagePackage.TOPIC__RELATED_TO:
				getRelatedTo().clear();
				getRelatedTo().addAll((Collection<? extends Topic>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MindMapLanguagePackage.TOPIC__SUBTOPICS:
				getSubtopics().clear();
				return;
			case MindMapLanguagePackage.TOPIC__ORDER:
				setOrder(ORDER_EDEFAULT);
				return;
			case MindMapLanguagePackage.TOPIC__EMPHASIZED:
				setEmphasized(EMPHASIZED_EDEFAULT);
				return;
			case MindMapLanguagePackage.TOPIC__COLOR:
				setColor((Color)null);
				return;
			case MindMapLanguagePackage.TOPIC__RELATED_TO:
				getRelatedTo().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MindMapLanguagePackage.TOPIC__SUBTOPICS:
				return subtopics != null && !subtopics.isEmpty();
			case MindMapLanguagePackage.TOPIC__ORDER:
				return order != ORDER_EDEFAULT;
			case MindMapLanguagePackage.TOPIC__EMPHASIZED:
				return emphasized != EMPHASIZED_EDEFAULT;
			case MindMapLanguagePackage.TOPIC__COLOR:
				return color != null;
			case MindMapLanguagePackage.TOPIC__RELATED_TO:
				return relatedTo != null && !relatedTo.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (order: ");
		result.append(order);
		result.append(", emphasized: ");
		result.append(emphasized);
		result.append(')');
		return result.toString();
	}

} //TopicImpl
