/**
 */
package MindMapLanguage.impl;

import MindMapLanguage.Color;
import MindMapLanguage.MindMap;
import MindMapLanguage.MindMapLanguagePackage;
import MindMapLanguage.Model;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link MindMapLanguage.impl.ModelImpl#getEditorVersion <em>Editor Version</em>}</li>
 *   <li>{@link MindMapLanguage.impl.ModelImpl#getMindmap <em>Mindmap</em>}</li>
 *   <li>{@link MindMapLanguage.impl.ModelImpl#getColors <em>Colors</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ModelImpl extends MinimalEObjectImpl.Container implements Model {
	/**
	 * The default value of the '{@link #getEditorVersion() <em>Editor Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEditorVersion()
	 * @generated
	 * @ordered
	 */
	protected static final int EDITOR_VERSION_EDEFAULT = 1;

	/**
	 * The cached value of the '{@link #getEditorVersion() <em>Editor Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEditorVersion()
	 * @generated
	 * @ordered
	 */
	protected int editorVersion = EDITOR_VERSION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getMindmap() <em>Mindmap</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMindmap()
	 * @generated
	 * @ordered
	 */
	protected EList<MindMap> mindmap;

	/**
	 * The cached value of the '{@link #getColors() <em>Colors</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColors()
	 * @generated
	 * @ordered
	 */
	protected EList<Color> colors;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MindMapLanguagePackage.Literals.MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getEditorVersion() {
		return editorVersion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEditorVersion(int newEditorVersion) {
		int oldEditorVersion = editorVersion;
		editorVersion = newEditorVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MindMapLanguagePackage.MODEL__EDITOR_VERSION, oldEditorVersion, editorVersion));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MindMap> getMindmap() {
		if (mindmap == null) {
			mindmap = new EObjectContainmentEList<MindMap>(MindMap.class, this, MindMapLanguagePackage.MODEL__MINDMAP);
		}
		return mindmap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Color> getColors() {
		if (colors == null) {
			colors = new EObjectContainmentEList<Color>(Color.class, this, MindMapLanguagePackage.MODEL__COLORS);
		}
		return colors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MindMapLanguagePackage.MODEL__MINDMAP:
				return ((InternalEList<?>)getMindmap()).basicRemove(otherEnd, msgs);
			case MindMapLanguagePackage.MODEL__COLORS:
				return ((InternalEList<?>)getColors()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MindMapLanguagePackage.MODEL__EDITOR_VERSION:
				return getEditorVersion();
			case MindMapLanguagePackage.MODEL__MINDMAP:
				return getMindmap();
			case MindMapLanguagePackage.MODEL__COLORS:
				return getColors();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MindMapLanguagePackage.MODEL__EDITOR_VERSION:
				setEditorVersion((Integer)newValue);
				return;
			case MindMapLanguagePackage.MODEL__MINDMAP:
				getMindmap().clear();
				getMindmap().addAll((Collection<? extends MindMap>)newValue);
				return;
			case MindMapLanguagePackage.MODEL__COLORS:
				getColors().clear();
				getColors().addAll((Collection<? extends Color>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MindMapLanguagePackage.MODEL__EDITOR_VERSION:
				setEditorVersion(EDITOR_VERSION_EDEFAULT);
				return;
			case MindMapLanguagePackage.MODEL__MINDMAP:
				getMindmap().clear();
				return;
			case MindMapLanguagePackage.MODEL__COLORS:
				getColors().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MindMapLanguagePackage.MODEL__EDITOR_VERSION:
				return editorVersion != EDITOR_VERSION_EDEFAULT;
			case MindMapLanguagePackage.MODEL__MINDMAP:
				return mindmap != null && !mindmap.isEmpty();
			case MindMapLanguagePackage.MODEL__COLORS:
				return colors != null && !colors.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (editorVersion: ");
		result.append(editorVersion);
		result.append(')');
		return result.toString();
	}

} //ModelImpl
