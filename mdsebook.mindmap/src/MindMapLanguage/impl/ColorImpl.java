/**
 */
package MindMapLanguage.impl;

import MindMapLanguage.Color;
import MindMapLanguage.MindMapLanguagePackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Color</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link MindMapLanguage.impl.ColorImpl#getRgbcode <em>Rgbcode</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ColorImpl extends NamedElementImpl implements Color {
	/**
	 * The default value of the '{@link #getRgbcode() <em>Rgbcode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRgbcode()
	 * @generated
	 * @ordered
	 */
	protected static final String RGBCODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRgbcode() <em>Rgbcode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRgbcode()
	 * @generated
	 * @ordered
	 */
	protected String rgbcode = RGBCODE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ColorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MindMapLanguagePackage.Literals.COLOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRgbcode() {
		return rgbcode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRgbcode(String newRgbcode) {
		String oldRgbcode = rgbcode;
		rgbcode = newRgbcode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MindMapLanguagePackage.COLOR__RGBCODE, oldRgbcode, rgbcode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MindMapLanguagePackage.COLOR__RGBCODE:
				return getRgbcode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MindMapLanguagePackage.COLOR__RGBCODE:
				setRgbcode((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MindMapLanguagePackage.COLOR__RGBCODE:
				setRgbcode(RGBCODE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MindMapLanguagePackage.COLOR__RGBCODE:
				return RGBCODE_EDEFAULT == null ? rgbcode != null : !RGBCODE_EDEFAULT.equals(rgbcode);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (rgbcode: ");
		result.append(rgbcode);
		result.append(')');
		return result.toString();
	}

} //ColorImpl
