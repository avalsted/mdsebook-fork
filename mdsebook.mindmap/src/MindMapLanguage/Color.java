/**
 */
package MindMapLanguage;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Color</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link MindMapLanguage.Color#getRgbcode <em>Rgbcode</em>}</li>
 * </ul>
 *
 * @see MindMapLanguage.MindMapLanguagePackage#getColor()
 * @model
 * @generated
 */
public interface Color extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Rgbcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rgbcode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rgbcode</em>' attribute.
	 * @see #setRgbcode(String)
	 * @see MindMapLanguage.MindMapLanguagePackage#getColor_Rgbcode()
	 * @model
	 * @generated
	 */
	String getRgbcode();

	/**
	 * Sets the value of the '{@link MindMapLanguage.Color#getRgbcode <em>Rgbcode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rgbcode</em>' attribute.
	 * @see #getRgbcode()
	 * @generated
	 */
	void setRgbcode(String value);

} // Color
