/**
 */
package MindMapLanguage;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Topic</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link MindMapLanguage.Topic#getSubtopics <em>Subtopics</em>}</li>
 *   <li>{@link MindMapLanguage.Topic#getOrder <em>Order</em>}</li>
 *   <li>{@link MindMapLanguage.Topic#isEmphasized <em>Emphasized</em>}</li>
 *   <li>{@link MindMapLanguage.Topic#getColor <em>Color</em>}</li>
 *   <li>{@link MindMapLanguage.Topic#getRelatedTo <em>Related To</em>}</li>
 * </ul>
 *
 * @see MindMapLanguage.MindMapLanguagePackage#getTopic()
 * @model
 * @generated
 */
public interface Topic extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Subtopics</b></em>' containment reference list.
	 * The list contents are of type {@link MindMapLanguage.Topic}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subtopics</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subtopics</em>' containment reference list.
	 * @see MindMapLanguage.MindMapLanguagePackage#getTopic_Subtopics()
	 * @model containment="true"
	 * @generated
	 */
	EList<Topic> getSubtopics();

	/**
	 * Returns the value of the '<em><b>Order</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Order</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Order</em>' attribute.
	 * @see #setOrder(int)
	 * @see MindMapLanguage.MindMapLanguagePackage#getTopic_Order()
	 * @model
	 * @generated
	 */
	int getOrder();

	/**
	 * Sets the value of the '{@link MindMapLanguage.Topic#getOrder <em>Order</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Order</em>' attribute.
	 * @see #getOrder()
	 * @generated
	 */
	void setOrder(int value);

	/**
	 * Returns the value of the '<em><b>Emphasized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Emphasized</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Emphasized</em>' attribute.
	 * @see #setEmphasized(boolean)
	 * @see MindMapLanguage.MindMapLanguagePackage#getTopic_Emphasized()
	 * @model
	 * @generated
	 */
	boolean isEmphasized();

	/**
	 * Sets the value of the '{@link MindMapLanguage.Topic#isEmphasized <em>Emphasized</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Emphasized</em>' attribute.
	 * @see #isEmphasized()
	 * @generated
	 */
	void setEmphasized(boolean value);

	/**
	 * Returns the value of the '<em><b>Color</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Color</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Color</em>' reference.
	 * @see #setColor(Color)
	 * @see MindMapLanguage.MindMapLanguagePackage#getTopic_Color()
	 * @model
	 * @generated
	 */
	Color getColor();

	/**
	 * Sets the value of the '{@link MindMapLanguage.Topic#getColor <em>Color</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Color</em>' reference.
	 * @see #getColor()
	 * @generated
	 */
	void setColor(Color value);

	/**
	 * Returns the value of the '<em><b>Related To</b></em>' reference list.
	 * The list contents are of type {@link MindMapLanguage.Topic}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Related To</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Related To</em>' reference list.
	 * @see MindMapLanguage.MindMapLanguagePackage#getTopic_RelatedTo()
	 * @model
	 * @generated
	 */
	EList<Topic> getRelatedTo();

} // Topic
