// (c) mdsebook, wasowski, tberger
// We can get rid od this in some way or another if bug
// https://bugs.eclipse.org/bugs/show_bug.cgi?id=391532
// is fixed.

package mdsebook.fsm.xtext.generator;

import org.eclipse.xtext.generator.IFileSystemAccess2;
import org.eclipse.emf.ecore.resource.Resource;
import mdsebook.fsm.xtext.generator.FsmGeneratorScala$;

class ScalaXtendBridge {
	
	public final static void doGenerate (Resource resource, IFileSystemAccess2 fsa)
	{ FsmGeneratorScala$.MODULE$.doGenerate (resource,fsa); }

}
