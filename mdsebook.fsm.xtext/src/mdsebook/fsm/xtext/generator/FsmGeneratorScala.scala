// (c) mddbook, wasowski, tberger
// An example of code generation hook for Xtext implemented in Scala.
// Sadly, due to a bug in Xtend, we are calling this from Xtext via Xtend and Java :(
package mdsebook.fsm.xtext.generator

import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2

import scala.collection.JavaConversions._
import mdsebook.fsm.FiniteStateMachine
import mdsebook.fsm.scala.transforms.{Fsm2Java, Fsm2Dot}

object FsmGeneratorScala {

  def doGenerate(resource: Resource, fsa: IFileSystemAccess2): Unit =
    for (m <- resource.getAllContents collect { case m: FiniteStateMachine => m }) {
      fsa generateFile (m.getName.capitalize + ".java", Fsm2Java compileToJava m)
      fsa generateFile (m.getName.capitalize + ".dot", Fsm2Dot compileToDot m)
    }
  
  // FIXME: one day when we have more time, translate the following Xtend code to Scala, 
  // to automatically call graphviz (not clear how many readers install graphviz anyways)
  //
  // import org.eclipse.core.resources.ResourcesPlugin 
  //	// execute graphviz dot to render a PDF file
  //  val projectName = resource.URI.segment(1)
  //	val project = ResourcesPlugin.workspace.root.getProject(projectName)
  //	var path = new File(project.location + "/src-gen/")
  //	var cmd = #["dot", "-Tpdf", fname + ".dot", "-o", fname + ".pdf"]
  //	Runtime.runtime.exec(cmd, null, path).wait

}