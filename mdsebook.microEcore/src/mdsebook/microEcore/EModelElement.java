/**
 */
package mdsebook.microEcore;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EModel Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see mdsebook.microEcore.MicroEcorePackage#getEModelElement()
 * @model abstract="true"
 * @generated
 */
public interface EModelElement extends EObject {
} // EModelElement
