/**
 */
package mdsebook.featuremodels1.util;

import mdsebook.featuremodels1.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see mdsebook.featuremodels1.FeatureModels1Package
 * @generated
 */
public class FeatureModels1Switch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static FeatureModels1Package modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureModels1Switch() {
		if (modelPackage == null) {
			modelPackage = FeatureModels1Package.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case FeatureModels1Package.MODEL1: {
				Model1 model1 = (Model1)theEObject;
				T result = caseModel1(model1);
				if (result == null) result = caseNamedElement1(model1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FeatureModels1Package.NAMED_ELEMENT1: {
				NamedElement1 namedElement1 = (NamedElement1)theEObject;
				T result = caseNamedElement1(namedElement1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FeatureModels1Package.FEATURE1: {
				Feature1 feature1 = (Feature1)theEObject;
				T result = caseFeature1(feature1);
				if (result == null) result = caseNamedElement1(feature1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FeatureModels1Package.GROUP1: {
				Group1 group1 = (Group1)theEObject;
				T result = caseGroup1(group1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FeatureModels1Package.OR_GROUP1: {
				OrGroup1 orGroup1 = (OrGroup1)theEObject;
				T result = caseOrGroup1(orGroup1);
				if (result == null) result = caseGroup1(orGroup1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FeatureModels1Package.XOR_GROUP1: {
				XorGroup1 xorGroup1 = (XorGroup1)theEObject;
				T result = caseXorGroup1(xorGroup1);
				if (result == null) result = caseGroup1(xorGroup1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModel1(Model1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedElement1(NamedElement1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Feature1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Feature1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFeature1(Feature1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Group1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Group1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGroup1(Group1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Or Group1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Or Group1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOrGroup1(OrGroup1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Xor Group1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Xor Group1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXorGroup1(XorGroup1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //FeatureModels1Switch
