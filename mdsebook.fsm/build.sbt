// (c) mddbook, wasowski, tberger

lazy val cleanGeneratedEcoreCode = taskKey[Unit]("Deleted generated ecore code")

name := "mdsebook.fsm"

javaSource in Compile := baseDirectory.value / "src"

libraryDependencies += "org.eclipse.emf" % "org.eclipse.emf.ecore" % "2.12.+"

libraryDependencies += "org.eclipse.emf" % "org.eclipse.emf.ecore.xmi" % "2.12.+"

libraryDependencies += "org.eclipse.emf" % "org.eclipse.emf.common" % "2.12.+"

/*generateEcoreCode := (mdsebook / mdsebook_cmd / run).value*/


// cleanGeneratedEcoreCode := { println ("we should delete generated code here") }
// 
// (clean in Clean) := { ((clean in Clean) dependsOn cleanGeneratedCode).value }

// def execute (cmd: String) (state: State) :State = {
//   println ("TEST!")
//   state.log.info ("HELLO!")
//   state.copy (remainingCommands = Exec(cmd, None) +: state.remainingCommands ) 
// }

/*generateEcoreCode := { execute ("mdsebook_cmd/run") _ }*/
/*generateEcoreCode := { val two = (run in Compile).partialInput("mdsebook_cmd/run").evaluated }*/
/*state.copy (remainingCommands = Exec("mdsebook_cmd/run", None) +: state.remainingCommands ) } }*/

  // Some("platform:/resource/mdsebook.fsm/model/fsm.genmodel"))  } }

  //state.log.info("Hello!");
/*(compile in Compile) := { ((compile in Compile) dependsOn generateEcoreCode).value }*/
// sourceGenerators in Compile += Exec("mdsebook_cmd/run", None)

