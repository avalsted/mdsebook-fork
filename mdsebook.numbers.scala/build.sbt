// (c) mddbook, wasowski, tberger

lazy val root = (project in file(".")).settings (

  organization := "mdsebook",
  name := "mdsebook.numbers.scala",
  version := "0.03",
  // stick to Scala version supported by Scala IDE 
  scalaVersion := "2.11.11",
  retrieveManaged := true,
  EclipseKeys.relativizeLibs := true,

  scalacOptions += "-deprecation",
  scalacOptions += "-feature",
  scalacOptions += "-language:implicitConversions",

  libraryDependencies += "org.scalatest" %% "scalatest" % "2.2.6" % "test"
)
