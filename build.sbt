// (c) mddbook, wasowski, tberger
name := "mdsebook"


lazy val commonSettings = Seq (
    organization := "mdsebook",
    version := "0.03",
    scalaVersion := "2.12.3",
    scalacOptions += "-deprecation",
    scalacOptions += "-feature",
    scalacOptions += "-language:implicitConversions",
    sourcesInBase := false
)

lazy val generateEcoreCode = taskKey[Unit]("Generate ecore model code")

// sub projects

lazy val mdsebook_cmd = (project in file("mdsebook.cmd"))
  .settings (commonSettings)

lazy val mdsebook_fsm = (project in file("mdsebook.fsm"))
  .dependsOn ( mdsebook_cmd % "compile")
  .settings (
    commonSettings,

    generateEcoreCode := 
      (mdsebook_cmd / Compile / run)
        .toTask(" platform:/resource/mdsebook.fsm/model/fsm.genmodel")
        .value,

    Compile / compile := 
      (Compile / compile)
        .dependsOn (generateEcoreCode)
        .value
  )

lazy val mdsebook_scala = (project in file("mdsebook.scala"))
  .dependsOn (mdsebook_fsm % "test")
  .settings (commonSettings)

