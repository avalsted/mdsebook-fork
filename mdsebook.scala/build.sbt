// (c) mddbook, wasowski, tberger

name := "mdsebook.scala"

unmanagedSourceDirectories in Test += baseDirectory.value / "../mdsebook.testModels/src/"

libraryDependencies += "org.eclipse.emf" % "org.eclipse.emf.ecore" % "2.12.+"

libraryDependencies += "org.eclipse.emf" % "org.eclipse.emf.ecore.xmi" % "2.12.+"

libraryDependencies += "org.eclipse.emf" % "org.eclipse.emf.common" % "2.12.+"

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % "test"
