// (c) mddbook, wasowski, tberger
// Example constraints implemented for the FSM models in Scala
// This is the main runner for the constraints example
// Run using 'sbt run'
package mdsebook.robot.scala

import mdsebook.scala.EMFScala._
import mdsebook.robot.RobotPackage
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl



object Main extends App {

  // register a resource factory for XMI files
  Resource.Factory.Registry.INSTANCE.
    getExtensionToFactoryMap.put("xmi", new XMIResourceFactoryImpl)

  // register the package magic (impure)
  RobotPackage.eINSTANCE.eClass

  // load the XMI file
  val uri: URI = URI.createURI ("../mdsebook.robot/test-files/random-walk-broken.robot.xmi") // <-- change file name here
  //val uri: URI = URI.createURI ("../mdsebook.robot/test-files/random-walk.robot.xmi") // <-- change file name here
  val resource: Resource = (new ResourceSetImpl).getResource (uri, true)

  val content :List[EObject] =
    EcoreUtil.getAllProperContents[EObject] (resource, false).toList

  if (content exists { eo => !Constraints.allModesInitialized.check(eo) })
    println ("Static semantics error: At least one non-empty mode has no initial submode!")
    
  if (content exists { eo => !Constraints.nonOverlappingTriggers.check(eo) })
    println ("Static semantics error: Some triggers overlap!")
    

}
