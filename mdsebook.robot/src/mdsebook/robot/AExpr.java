/**
 */
package mdsebook.robot;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AExpr</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see mdsebook.robot.RobotPackage#getAExpr()
 * @model abstract="true"
 * @generated
 */
public interface AExpr extends EObject {
} // AExpr
