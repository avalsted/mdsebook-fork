/**
 */
package mdsebook.robot;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ac Turn</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mdsebook.robot.AcTurn#getAngle <em>Angle</em>}</li>
 *   <li>{@link mdsebook.robot.AcTurn#isRight <em>Right</em>}</li>
 * </ul>
 *
 * @see mdsebook.robot.RobotPackage#getAcTurn()
 * @model
 * @generated
 */
public interface AcTurn extends Action {
	/**
	 * Returns the value of the '<em><b>Angle</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Angle</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Angle</em>' containment reference.
	 * @see #setAngle(AExpr)
	 * @see mdsebook.robot.RobotPackage#getAcTurn_Angle()
	 * @model containment="true"
	 * @generated
	 */
	AExpr getAngle();

	/**
	 * Sets the value of the '{@link mdsebook.robot.AcTurn#getAngle <em>Angle</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Angle</em>' containment reference.
	 * @see #getAngle()
	 * @generated
	 */
	void setAngle(AExpr value);

	/**
	 * Returns the value of the '<em><b>Right</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Right</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right</em>' attribute.
	 * @see #setRight(boolean)
	 * @see mdsebook.robot.RobotPackage#getAcTurn_Right()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isRight();

	/**
	 * Sets the value of the '{@link mdsebook.robot.AcTurn#isRight <em>Right</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right</em>' attribute.
	 * @see #isRight()
	 * @generated
	 */
	void setRight(boolean value);

} // AcTurn
