/**
 */
package mdsebook.robot;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ev Obstacle</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see mdsebook.robot.RobotPackage#getEvObstacle()
 * @model
 * @generated
 */
public interface EvObstacle extends EObject {
} // EvObstacle
