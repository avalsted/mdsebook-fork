/**
 */
package mdsebook.robot;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bin Expr</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mdsebook.robot.BinExpr#getOpe <em>Ope</em>}</li>
 *   <li>{@link mdsebook.robot.BinExpr#getLeft <em>Left</em>}</li>
 *   <li>{@link mdsebook.robot.BinExpr#getRight <em>Right</em>}</li>
 * </ul>
 *
 * @see mdsebook.robot.RobotPackage#getBinExpr()
 * @model
 * @generated
 */
public interface BinExpr extends AExpr {
	/**
	 * Returns the value of the '<em><b>Ope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ope</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ope</em>' attribute.
	 * @see #setOpe(String)
	 * @see mdsebook.robot.RobotPackage#getBinExpr_Ope()
	 * @model
	 * @generated
	 */
	String getOpe();

	/**
	 * Sets the value of the '{@link mdsebook.robot.BinExpr#getOpe <em>Ope</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ope</em>' attribute.
	 * @see #getOpe()
	 * @generated
	 */
	void setOpe(String value);

	/**
	 * Returns the value of the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Left</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left</em>' containment reference.
	 * @see #setLeft(AExpr)
	 * @see mdsebook.robot.RobotPackage#getBinExpr_Left()
	 * @model containment="true"
	 * @generated
	 */
	AExpr getLeft();

	/**
	 * Sets the value of the '{@link mdsebook.robot.BinExpr#getLeft <em>Left</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Left</em>' containment reference.
	 * @see #getLeft()
	 * @generated
	 */
	void setLeft(AExpr value);

	/**
	 * Returns the value of the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Right</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right</em>' containment reference.
	 * @see #setRight(AExpr)
	 * @see mdsebook.robot.RobotPackage#getBinExpr_Right()
	 * @model containment="true"
	 * @generated
	 */
	AExpr getRight();

	/**
	 * Sets the value of the '{@link mdsebook.robot.BinExpr#getRight <em>Right</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right</em>' containment reference.
	 * @see #getRight()
	 * @generated
	 */
	void setRight(AExpr value);

} // BinExpr
