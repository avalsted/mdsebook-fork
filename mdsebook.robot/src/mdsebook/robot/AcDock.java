/**
 */
package mdsebook.robot;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ac Dock</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see mdsebook.robot.RobotPackage#getAcDock()
 * @model
 * @generated
 */
public interface AcDock extends Action {
} // AcDock
