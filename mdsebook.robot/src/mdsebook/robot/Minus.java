/**
 */
package mdsebook.robot;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Minus</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mdsebook.robot.Minus#getAexpr <em>Aexpr</em>}</li>
 * </ul>
 *
 * @see mdsebook.robot.RobotPackage#getMinus()
 * @model
 * @generated
 */
public interface Minus extends AExpr {
	/**
	 * Returns the value of the '<em><b>Aexpr</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Aexpr</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aexpr</em>' containment reference.
	 * @see #setAexpr(AExpr)
	 * @see mdsebook.robot.RobotPackage#getMinus_Aexpr()
	 * @model containment="true" required="true"
	 * @generated
	 */
	AExpr getAexpr();

	/**
	 * Sets the value of the '{@link mdsebook.robot.Minus#getAexpr <em>Aexpr</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Aexpr</em>' containment reference.
	 * @see #getAexpr()
	 * @generated
	 */
	void setAexpr(AExpr value);

} // Minus
