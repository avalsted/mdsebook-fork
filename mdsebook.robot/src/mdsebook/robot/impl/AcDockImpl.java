/**
 */
package mdsebook.robot.impl;

import mdsebook.robot.AcDock;
import mdsebook.robot.RobotPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ac Dock</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AcDockImpl extends ActionImpl implements AcDock {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AcDockImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RobotPackage.Literals.AC_DOCK;
	}

} //AcDockImpl
