/**
 */
package mdsebook.robot.impl;

import mdsebook.robot.ModeProperty;
import mdsebook.robot.RobotPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Mode Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class ModePropertyImpl extends MinimalEObjectImpl.Container implements ModeProperty {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ModePropertyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RobotPackage.Literals.MODE_PROPERTY;
	}

} //ModePropertyImpl
