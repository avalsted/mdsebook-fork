/**
 */
package mdsebook.robot.impl;

import mdsebook.robot.EvObstacle;
import mdsebook.robot.RobotPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ev Obstacle</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class EvObstacleImpl extends MinimalEObjectImpl.Container implements EvObstacle {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EvObstacleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RobotPackage.Literals.EV_OBSTACLE;
	}

} //EvObstacleImpl
