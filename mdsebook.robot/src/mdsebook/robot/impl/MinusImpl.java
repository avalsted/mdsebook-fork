/**
 */
package mdsebook.robot.impl;

import mdsebook.robot.AExpr;
import mdsebook.robot.Minus;
import mdsebook.robot.RobotPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Minus</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mdsebook.robot.impl.MinusImpl#getAexpr <em>Aexpr</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MinusImpl extends AExprImpl implements Minus {
	/**
	 * The cached value of the '{@link #getAexpr() <em>Aexpr</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAexpr()
	 * @generated
	 * @ordered
	 */
	protected AExpr aexpr;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MinusImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RobotPackage.Literals.MINUS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AExpr getAexpr() {
		return aexpr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAexpr(AExpr newAexpr, NotificationChain msgs) {
		AExpr oldAexpr = aexpr;
		aexpr = newAexpr;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, RobotPackage.MINUS__AEXPR, oldAexpr, newAexpr);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAexpr(AExpr newAexpr) {
		if (newAexpr != aexpr) {
			NotificationChain msgs = null;
			if (aexpr != null)
				msgs = ((InternalEObject)aexpr).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - RobotPackage.MINUS__AEXPR, null, msgs);
			if (newAexpr != null)
				msgs = ((InternalEObject)newAexpr).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - RobotPackage.MINUS__AEXPR, null, msgs);
			msgs = basicSetAexpr(newAexpr, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RobotPackage.MINUS__AEXPR, newAexpr, newAexpr));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RobotPackage.MINUS__AEXPR:
				return basicSetAexpr(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RobotPackage.MINUS__AEXPR:
				return getAexpr();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RobotPackage.MINUS__AEXPR:
				setAexpr((AExpr)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RobotPackage.MINUS__AEXPR:
				setAexpr((AExpr)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RobotPackage.MINUS__AEXPR:
				return aexpr != null;
		}
		return super.eIsSet(featureID);
	}

} //MinusImpl
