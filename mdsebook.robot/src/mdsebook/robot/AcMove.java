/**
 */
package mdsebook.robot;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ac Move</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mdsebook.robot.AcMove#getDistance <em>Distance</em>}</li>
 *   <li>{@link mdsebook.robot.AcMove#isForward <em>Forward</em>}</li>
 * </ul>
 *
 * @see mdsebook.robot.RobotPackage#getAcMove()
 * @model
 * @generated
 */
public interface AcMove extends Action {
	/**
	 * Returns the value of the '<em><b>Distance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Distance</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Distance</em>' containment reference.
	 * @see #setDistance(AExpr)
	 * @see mdsebook.robot.RobotPackage#getAcMove_Distance()
	 * @model containment="true"
	 * @generated
	 */
	AExpr getDistance();

	/**
	 * Sets the value of the '{@link mdsebook.robot.AcMove#getDistance <em>Distance</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Distance</em>' containment reference.
	 * @see #getDistance()
	 * @generated
	 */
	void setDistance(AExpr value);

	/**
	 * Returns the value of the '<em><b>Forward</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Forward</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Forward</em>' attribute.
	 * @see #setForward(boolean)
	 * @see mdsebook.robot.RobotPackage#getAcMove_Forward()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isForward();

	/**
	 * Sets the value of the '{@link mdsebook.robot.AcMove#isForward <em>Forward</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Forward</em>' attribute.
	 * @see #isForward()
	 * @generated
	 */
	void setForward(boolean value);

} // AcMove
