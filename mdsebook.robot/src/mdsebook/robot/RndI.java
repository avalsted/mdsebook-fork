/**
 */
package mdsebook.robot;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Rnd I</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mdsebook.robot.RndI#getMax <em>Max</em>}</li>
 *   <li>{@link mdsebook.robot.RndI#getMin <em>Min</em>}</li>
 * </ul>
 *
 * @see mdsebook.robot.RobotPackage#getRndI()
 * @model
 * @generated
 */
public interface RndI extends AExpr {
	/**
	 * Returns the value of the '<em><b>Max</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max</em>' containment reference.
	 * @see #setMax(AExpr)
	 * @see mdsebook.robot.RobotPackage#getRndI_Max()
	 * @model containment="true"
	 * @generated
	 */
	AExpr getMax();

	/**
	 * Sets the value of the '{@link mdsebook.robot.RndI#getMax <em>Max</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max</em>' containment reference.
	 * @see #getMax()
	 * @generated
	 */
	void setMax(AExpr value);

	/**
	 * Returns the value of the '<em><b>Min</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min</em>' containment reference.
	 * @see #setMin(AExpr)
	 * @see mdsebook.robot.RobotPackage#getRndI_Min()
	 * @model containment="true"
	 * @generated
	 */
	AExpr getMin();

	/**
	 * Sets the value of the '{@link mdsebook.robot.RndI#getMin <em>Min</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min</em>' containment reference.
	 * @see #getMin()
	 * @generated
	 */
	void setMin(AExpr value);

} // RndI
