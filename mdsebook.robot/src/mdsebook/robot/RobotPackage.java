/**
 */
package mdsebook.robot;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see mdsebook.robot.RobotFactory
 * @model kind="package"
 * @generated
 */
public interface RobotPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "robot";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.mdsebook.org/mdsebook.robot";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "mdsebook.robot";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RobotPackage eINSTANCE = mdsebook.robot.impl.RobotPackageImpl.init();

	/**
	 * The meta object id for the '{@link mdsebook.robot.impl.ModePropertyImpl <em>Mode Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mdsebook.robot.impl.ModePropertyImpl
	 * @see mdsebook.robot.impl.RobotPackageImpl#getModeProperty()
	 * @generated
	 */
	int MODE_PROPERTY = 1;

	/**
	 * The number of structural features of the '<em>Mode Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODE_PROPERTY_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Mode Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODE_PROPERTY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link mdsebook.robot.impl.ModeImpl <em>Mode</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mdsebook.robot.impl.ModeImpl
	 * @see mdsebook.robot.impl.RobotPackageImpl#getMode()
	 * @generated
	 */
	int MODE = 0;

	/**
	 * The feature id for the '<em><b>Actions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODE__ACTIONS = MODE_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Continuation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODE__CONTINUATION = MODE_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Modes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODE__MODES = MODE_PROPERTY_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Reactions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODE__REACTIONS = MODE_PROPERTY_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODE__NAME = MODE_PROPERTY_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Initial</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODE__INITIAL = MODE_PROPERTY_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Mode</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODE_FEATURE_COUNT = MODE_PROPERTY_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>Mode</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODE_OPERATION_COUNT = MODE_PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link mdsebook.robot.impl.ReactionImpl <em>Reaction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mdsebook.robot.impl.ReactionImpl
	 * @see mdsebook.robot.impl.RobotPackageImpl#getReaction()
	 * @generated
	 */
	int REACTION = 2;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTION__TARGET = MODE_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Trigger</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTION__TRIGGER = MODE_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Reaction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTION_FEATURE_COUNT = MODE_PROPERTY_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Reaction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTION_OPERATION_COUNT = MODE_PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link mdsebook.robot.impl.ActionImpl <em>Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mdsebook.robot.impl.ActionImpl
	 * @see mdsebook.robot.impl.RobotPackageImpl#getAction()
	 * @generated
	 */
	int ACTION = 3;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__DURATION = 0;

	/**
	 * The feature id for the '<em><b>Speed</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__SPEED = 1;

	/**
	 * The number of structural features of the '<em>Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link mdsebook.robot.impl.AcDockImpl <em>Ac Dock</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mdsebook.robot.impl.AcDockImpl
	 * @see mdsebook.robot.impl.RobotPackageImpl#getAcDock()
	 * @generated
	 */
	int AC_DOCK = 4;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_DOCK__DURATION = ACTION__DURATION;

	/**
	 * The feature id for the '<em><b>Speed</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_DOCK__SPEED = ACTION__SPEED;

	/**
	 * The number of structural features of the '<em>Ac Dock</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_DOCK_FEATURE_COUNT = ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Ac Dock</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_DOCK_OPERATION_COUNT = ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link mdsebook.robot.impl.AcMoveImpl <em>Ac Move</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mdsebook.robot.impl.AcMoveImpl
	 * @see mdsebook.robot.impl.RobotPackageImpl#getAcMove()
	 * @generated
	 */
	int AC_MOVE = 5;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_MOVE__DURATION = ACTION__DURATION;

	/**
	 * The feature id for the '<em><b>Speed</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_MOVE__SPEED = ACTION__SPEED;

	/**
	 * The feature id for the '<em><b>Distance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_MOVE__DISTANCE = ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Forward</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_MOVE__FORWARD = ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Ac Move</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_MOVE_FEATURE_COUNT = ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Ac Move</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_MOVE_OPERATION_COUNT = ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link mdsebook.robot.impl.AcTurnImpl <em>Ac Turn</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mdsebook.robot.impl.AcTurnImpl
	 * @see mdsebook.robot.impl.RobotPackageImpl#getAcTurn()
	 * @generated
	 */
	int AC_TURN = 6;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_TURN__DURATION = ACTION__DURATION;

	/**
	 * The feature id for the '<em><b>Speed</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_TURN__SPEED = ACTION__SPEED;

	/**
	 * The feature id for the '<em><b>Angle</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_TURN__ANGLE = ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Right</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_TURN__RIGHT = ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Ac Turn</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_TURN_FEATURE_COUNT = ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Ac Turn</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AC_TURN_OPERATION_COUNT = ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link mdsebook.robot.impl.EvObstacleImpl <em>Ev Obstacle</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mdsebook.robot.impl.EvObstacleImpl
	 * @see mdsebook.robot.impl.RobotPackageImpl#getEvObstacle()
	 * @generated
	 */
	int EV_OBSTACLE = 7;

	/**
	 * The number of structural features of the '<em>Ev Obstacle</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EV_OBSTACLE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Ev Obstacle</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EV_OBSTACLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link mdsebook.robot.impl.AExprImpl <em>AExpr</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mdsebook.robot.impl.AExprImpl
	 * @see mdsebook.robot.impl.RobotPackageImpl#getAExpr()
	 * @generated
	 */
	int AEXPR = 8;

	/**
	 * The number of structural features of the '<em>AExpr</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AEXPR_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>AExpr</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AEXPR_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link mdsebook.robot.impl.BinExprImpl <em>Bin Expr</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mdsebook.robot.impl.BinExprImpl
	 * @see mdsebook.robot.impl.RobotPackageImpl#getBinExpr()
	 * @generated
	 */
	int BIN_EXPR = 9;

	/**
	 * The feature id for the '<em><b>Ope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIN_EXPR__OPE = AEXPR_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIN_EXPR__LEFT = AEXPR_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIN_EXPR__RIGHT = AEXPR_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Bin Expr</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIN_EXPR_FEATURE_COUNT = AEXPR_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Bin Expr</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIN_EXPR_OPERATION_COUNT = AEXPR_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link mdsebook.robot.impl.MinusImpl <em>Minus</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mdsebook.robot.impl.MinusImpl
	 * @see mdsebook.robot.impl.RobotPackageImpl#getMinus()
	 * @generated
	 */
	int MINUS = 10;

	/**
	 * The feature id for the '<em><b>Aexpr</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINUS__AEXPR = AEXPR_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Minus</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINUS_FEATURE_COUNT = AEXPR_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Minus</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MINUS_OPERATION_COUNT = AEXPR_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link mdsebook.robot.impl.RndIImpl <em>Rnd I</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mdsebook.robot.impl.RndIImpl
	 * @see mdsebook.robot.impl.RobotPackageImpl#getRndI()
	 * @generated
	 */
	int RND_I = 11;

	/**
	 * The feature id for the '<em><b>Max</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RND_I__MAX = AEXPR_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Min</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RND_I__MIN = AEXPR_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Rnd I</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RND_I_FEATURE_COUNT = AEXPR_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Rnd I</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RND_I_OPERATION_COUNT = AEXPR_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link mdsebook.robot.impl.CstIImpl <em>Cst I</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mdsebook.robot.impl.CstIImpl
	 * @see mdsebook.robot.impl.RobotPackageImpl#getCstI()
	 * @generated
	 */
	int CST_I = 12;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CST_I__VALUE = AEXPR_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Cst I</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CST_I_FEATURE_COUNT = AEXPR_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Cst I</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CST_I_OPERATION_COUNT = AEXPR_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link mdsebook.robot.Event <em>Event</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mdsebook.robot.Event
	 * @see mdsebook.robot.impl.RobotPackageImpl#getEvent()
	 * @generated
	 */
	int EVENT = 13;


	/**
	 * Returns the meta object for class '{@link mdsebook.robot.Mode <em>Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Mode</em>'.
	 * @see mdsebook.robot.Mode
	 * @generated
	 */
	EClass getMode();

	/**
	 * Returns the meta object for the containment reference list '{@link mdsebook.robot.Mode#getActions <em>Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Actions</em>'.
	 * @see mdsebook.robot.Mode#getActions()
	 * @see #getMode()
	 * @generated
	 */
	EReference getMode_Actions();

	/**
	 * Returns the meta object for the reference '{@link mdsebook.robot.Mode#getContinuation <em>Continuation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Continuation</em>'.
	 * @see mdsebook.robot.Mode#getContinuation()
	 * @see #getMode()
	 * @generated
	 */
	EReference getMode_Continuation();

	/**
	 * Returns the meta object for the containment reference list '{@link mdsebook.robot.Mode#getModes <em>Modes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Modes</em>'.
	 * @see mdsebook.robot.Mode#getModes()
	 * @see #getMode()
	 * @generated
	 */
	EReference getMode_Modes();

	/**
	 * Returns the meta object for the containment reference list '{@link mdsebook.robot.Mode#getReactions <em>Reactions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Reactions</em>'.
	 * @see mdsebook.robot.Mode#getReactions()
	 * @see #getMode()
	 * @generated
	 */
	EReference getMode_Reactions();

	/**
	 * Returns the meta object for the attribute '{@link mdsebook.robot.Mode#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see mdsebook.robot.Mode#getName()
	 * @see #getMode()
	 * @generated
	 */
	EAttribute getMode_Name();

	/**
	 * Returns the meta object for the attribute '{@link mdsebook.robot.Mode#isInitial <em>Initial</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Initial</em>'.
	 * @see mdsebook.robot.Mode#isInitial()
	 * @see #getMode()
	 * @generated
	 */
	EAttribute getMode_Initial();

	/**
	 * Returns the meta object for class '{@link mdsebook.robot.ModeProperty <em>Mode Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Mode Property</em>'.
	 * @see mdsebook.robot.ModeProperty
	 * @generated
	 */
	EClass getModeProperty();

	/**
	 * Returns the meta object for class '{@link mdsebook.robot.Reaction <em>Reaction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reaction</em>'.
	 * @see mdsebook.robot.Reaction
	 * @generated
	 */
	EClass getReaction();

	/**
	 * Returns the meta object for the reference '{@link mdsebook.robot.Reaction#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see mdsebook.robot.Reaction#getTarget()
	 * @see #getReaction()
	 * @generated
	 */
	EReference getReaction_Target();

	/**
	 * Returns the meta object for the attribute '{@link mdsebook.robot.Reaction#getTrigger <em>Trigger</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Trigger</em>'.
	 * @see mdsebook.robot.Reaction#getTrigger()
	 * @see #getReaction()
	 * @generated
	 */
	EAttribute getReaction_Trigger();

	/**
	 * Returns the meta object for class '{@link mdsebook.robot.Action <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Action</em>'.
	 * @see mdsebook.robot.Action
	 * @generated
	 */
	EClass getAction();

	/**
	 * Returns the meta object for the containment reference '{@link mdsebook.robot.Action#getDuration <em>Duration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Duration</em>'.
	 * @see mdsebook.robot.Action#getDuration()
	 * @see #getAction()
	 * @generated
	 */
	EReference getAction_Duration();

	/**
	 * Returns the meta object for the containment reference '{@link mdsebook.robot.Action#getSpeed <em>Speed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Speed</em>'.
	 * @see mdsebook.robot.Action#getSpeed()
	 * @see #getAction()
	 * @generated
	 */
	EReference getAction_Speed();

	/**
	 * Returns the meta object for class '{@link mdsebook.robot.AcDock <em>Ac Dock</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ac Dock</em>'.
	 * @see mdsebook.robot.AcDock
	 * @generated
	 */
	EClass getAcDock();

	/**
	 * Returns the meta object for class '{@link mdsebook.robot.AcMove <em>Ac Move</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ac Move</em>'.
	 * @see mdsebook.robot.AcMove
	 * @generated
	 */
	EClass getAcMove();

	/**
	 * Returns the meta object for the containment reference '{@link mdsebook.robot.AcMove#getDistance <em>Distance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Distance</em>'.
	 * @see mdsebook.robot.AcMove#getDistance()
	 * @see #getAcMove()
	 * @generated
	 */
	EReference getAcMove_Distance();

	/**
	 * Returns the meta object for the attribute '{@link mdsebook.robot.AcMove#isForward <em>Forward</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Forward</em>'.
	 * @see mdsebook.robot.AcMove#isForward()
	 * @see #getAcMove()
	 * @generated
	 */
	EAttribute getAcMove_Forward();

	/**
	 * Returns the meta object for class '{@link mdsebook.robot.AcTurn <em>Ac Turn</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ac Turn</em>'.
	 * @see mdsebook.robot.AcTurn
	 * @generated
	 */
	EClass getAcTurn();

	/**
	 * Returns the meta object for the containment reference '{@link mdsebook.robot.AcTurn#getAngle <em>Angle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Angle</em>'.
	 * @see mdsebook.robot.AcTurn#getAngle()
	 * @see #getAcTurn()
	 * @generated
	 */
	EReference getAcTurn_Angle();

	/**
	 * Returns the meta object for the attribute '{@link mdsebook.robot.AcTurn#isRight <em>Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Right</em>'.
	 * @see mdsebook.robot.AcTurn#isRight()
	 * @see #getAcTurn()
	 * @generated
	 */
	EAttribute getAcTurn_Right();

	/**
	 * Returns the meta object for class '{@link mdsebook.robot.EvObstacle <em>Ev Obstacle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ev Obstacle</em>'.
	 * @see mdsebook.robot.EvObstacle
	 * @generated
	 */
	EClass getEvObstacle();

	/**
	 * Returns the meta object for class '{@link mdsebook.robot.AExpr <em>AExpr</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AExpr</em>'.
	 * @see mdsebook.robot.AExpr
	 * @generated
	 */
	EClass getAExpr();

	/**
	 * Returns the meta object for class '{@link mdsebook.robot.BinExpr <em>Bin Expr</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Bin Expr</em>'.
	 * @see mdsebook.robot.BinExpr
	 * @generated
	 */
	EClass getBinExpr();

	/**
	 * Returns the meta object for the attribute '{@link mdsebook.robot.BinExpr#getOpe <em>Ope</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ope</em>'.
	 * @see mdsebook.robot.BinExpr#getOpe()
	 * @see #getBinExpr()
	 * @generated
	 */
	EAttribute getBinExpr_Ope();

	/**
	 * Returns the meta object for the containment reference '{@link mdsebook.robot.BinExpr#getLeft <em>Left</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left</em>'.
	 * @see mdsebook.robot.BinExpr#getLeft()
	 * @see #getBinExpr()
	 * @generated
	 */
	EReference getBinExpr_Left();

	/**
	 * Returns the meta object for the containment reference '{@link mdsebook.robot.BinExpr#getRight <em>Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right</em>'.
	 * @see mdsebook.robot.BinExpr#getRight()
	 * @see #getBinExpr()
	 * @generated
	 */
	EReference getBinExpr_Right();

	/**
	 * Returns the meta object for class '{@link mdsebook.robot.Minus <em>Minus</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Minus</em>'.
	 * @see mdsebook.robot.Minus
	 * @generated
	 */
	EClass getMinus();

	/**
	 * Returns the meta object for the containment reference '{@link mdsebook.robot.Minus#getAexpr <em>Aexpr</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Aexpr</em>'.
	 * @see mdsebook.robot.Minus#getAexpr()
	 * @see #getMinus()
	 * @generated
	 */
	EReference getMinus_Aexpr();

	/**
	 * Returns the meta object for class '{@link mdsebook.robot.RndI <em>Rnd I</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rnd I</em>'.
	 * @see mdsebook.robot.RndI
	 * @generated
	 */
	EClass getRndI();

	/**
	 * Returns the meta object for the containment reference '{@link mdsebook.robot.RndI#getMax <em>Max</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Max</em>'.
	 * @see mdsebook.robot.RndI#getMax()
	 * @see #getRndI()
	 * @generated
	 */
	EReference getRndI_Max();

	/**
	 * Returns the meta object for the containment reference '{@link mdsebook.robot.RndI#getMin <em>Min</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Min</em>'.
	 * @see mdsebook.robot.RndI#getMin()
	 * @see #getRndI()
	 * @generated
	 */
	EReference getRndI_Min();

	/**
	 * Returns the meta object for class '{@link mdsebook.robot.CstI <em>Cst I</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cst I</em>'.
	 * @see mdsebook.robot.CstI
	 * @generated
	 */
	EClass getCstI();

	/**
	 * Returns the meta object for the attribute '{@link mdsebook.robot.CstI#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see mdsebook.robot.CstI#getValue()
	 * @see #getCstI()
	 * @generated
	 */
	EAttribute getCstI_Value();

	/**
	 * Returns the meta object for enum '{@link mdsebook.robot.Event <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Event</em>'.
	 * @see mdsebook.robot.Event
	 * @generated
	 */
	EEnum getEvent();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	RobotFactory getRobotFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link mdsebook.robot.impl.ModeImpl <em>Mode</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mdsebook.robot.impl.ModeImpl
		 * @see mdsebook.robot.impl.RobotPackageImpl#getMode()
		 * @generated
		 */
		EClass MODE = eINSTANCE.getMode();

		/**
		 * The meta object literal for the '<em><b>Actions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODE__ACTIONS = eINSTANCE.getMode_Actions();

		/**
		 * The meta object literal for the '<em><b>Continuation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODE__CONTINUATION = eINSTANCE.getMode_Continuation();

		/**
		 * The meta object literal for the '<em><b>Modes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODE__MODES = eINSTANCE.getMode_Modes();

		/**
		 * The meta object literal for the '<em><b>Reactions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODE__REACTIONS = eINSTANCE.getMode_Reactions();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODE__NAME = eINSTANCE.getMode_Name();

		/**
		 * The meta object literal for the '<em><b>Initial</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODE__INITIAL = eINSTANCE.getMode_Initial();

		/**
		 * The meta object literal for the '{@link mdsebook.robot.impl.ModePropertyImpl <em>Mode Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mdsebook.robot.impl.ModePropertyImpl
		 * @see mdsebook.robot.impl.RobotPackageImpl#getModeProperty()
		 * @generated
		 */
		EClass MODE_PROPERTY = eINSTANCE.getModeProperty();

		/**
		 * The meta object literal for the '{@link mdsebook.robot.impl.ReactionImpl <em>Reaction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mdsebook.robot.impl.ReactionImpl
		 * @see mdsebook.robot.impl.RobotPackageImpl#getReaction()
		 * @generated
		 */
		EClass REACTION = eINSTANCE.getReaction();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTION__TARGET = eINSTANCE.getReaction_Target();

		/**
		 * The meta object literal for the '<em><b>Trigger</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REACTION__TRIGGER = eINSTANCE.getReaction_Trigger();

		/**
		 * The meta object literal for the '{@link mdsebook.robot.impl.ActionImpl <em>Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mdsebook.robot.impl.ActionImpl
		 * @see mdsebook.robot.impl.RobotPackageImpl#getAction()
		 * @generated
		 */
		EClass ACTION = eINSTANCE.getAction();

		/**
		 * The meta object literal for the '<em><b>Duration</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTION__DURATION = eINSTANCE.getAction_Duration();

		/**
		 * The meta object literal for the '<em><b>Speed</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTION__SPEED = eINSTANCE.getAction_Speed();

		/**
		 * The meta object literal for the '{@link mdsebook.robot.impl.AcDockImpl <em>Ac Dock</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mdsebook.robot.impl.AcDockImpl
		 * @see mdsebook.robot.impl.RobotPackageImpl#getAcDock()
		 * @generated
		 */
		EClass AC_DOCK = eINSTANCE.getAcDock();

		/**
		 * The meta object literal for the '{@link mdsebook.robot.impl.AcMoveImpl <em>Ac Move</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mdsebook.robot.impl.AcMoveImpl
		 * @see mdsebook.robot.impl.RobotPackageImpl#getAcMove()
		 * @generated
		 */
		EClass AC_MOVE = eINSTANCE.getAcMove();

		/**
		 * The meta object literal for the '<em><b>Distance</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AC_MOVE__DISTANCE = eINSTANCE.getAcMove_Distance();

		/**
		 * The meta object literal for the '<em><b>Forward</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AC_MOVE__FORWARD = eINSTANCE.getAcMove_Forward();

		/**
		 * The meta object literal for the '{@link mdsebook.robot.impl.AcTurnImpl <em>Ac Turn</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mdsebook.robot.impl.AcTurnImpl
		 * @see mdsebook.robot.impl.RobotPackageImpl#getAcTurn()
		 * @generated
		 */
		EClass AC_TURN = eINSTANCE.getAcTurn();

		/**
		 * The meta object literal for the '<em><b>Angle</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AC_TURN__ANGLE = eINSTANCE.getAcTurn_Angle();

		/**
		 * The meta object literal for the '<em><b>Right</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AC_TURN__RIGHT = eINSTANCE.getAcTurn_Right();

		/**
		 * The meta object literal for the '{@link mdsebook.robot.impl.EvObstacleImpl <em>Ev Obstacle</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mdsebook.robot.impl.EvObstacleImpl
		 * @see mdsebook.robot.impl.RobotPackageImpl#getEvObstacle()
		 * @generated
		 */
		EClass EV_OBSTACLE = eINSTANCE.getEvObstacle();

		/**
		 * The meta object literal for the '{@link mdsebook.robot.impl.AExprImpl <em>AExpr</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mdsebook.robot.impl.AExprImpl
		 * @see mdsebook.robot.impl.RobotPackageImpl#getAExpr()
		 * @generated
		 */
		EClass AEXPR = eINSTANCE.getAExpr();

		/**
		 * The meta object literal for the '{@link mdsebook.robot.impl.BinExprImpl <em>Bin Expr</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mdsebook.robot.impl.BinExprImpl
		 * @see mdsebook.robot.impl.RobotPackageImpl#getBinExpr()
		 * @generated
		 */
		EClass BIN_EXPR = eINSTANCE.getBinExpr();

		/**
		 * The meta object literal for the '<em><b>Ope</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BIN_EXPR__OPE = eINSTANCE.getBinExpr_Ope();

		/**
		 * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BIN_EXPR__LEFT = eINSTANCE.getBinExpr_Left();

		/**
		 * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BIN_EXPR__RIGHT = eINSTANCE.getBinExpr_Right();

		/**
		 * The meta object literal for the '{@link mdsebook.robot.impl.MinusImpl <em>Minus</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mdsebook.robot.impl.MinusImpl
		 * @see mdsebook.robot.impl.RobotPackageImpl#getMinus()
		 * @generated
		 */
		EClass MINUS = eINSTANCE.getMinus();

		/**
		 * The meta object literal for the '<em><b>Aexpr</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MINUS__AEXPR = eINSTANCE.getMinus_Aexpr();

		/**
		 * The meta object literal for the '{@link mdsebook.robot.impl.RndIImpl <em>Rnd I</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mdsebook.robot.impl.RndIImpl
		 * @see mdsebook.robot.impl.RobotPackageImpl#getRndI()
		 * @generated
		 */
		EClass RND_I = eINSTANCE.getRndI();

		/**
		 * The meta object literal for the '<em><b>Max</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RND_I__MAX = eINSTANCE.getRndI_Max();

		/**
		 * The meta object literal for the '<em><b>Min</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RND_I__MIN = eINSTANCE.getRndI_Min();

		/**
		 * The meta object literal for the '{@link mdsebook.robot.impl.CstIImpl <em>Cst I</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mdsebook.robot.impl.CstIImpl
		 * @see mdsebook.robot.impl.RobotPackageImpl#getCstI()
		 * @generated
		 */
		EClass CST_I = eINSTANCE.getCstI();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CST_I__VALUE = eINSTANCE.getCstI_Value();

		/**
		 * The meta object literal for the '{@link mdsebook.robot.Event <em>Event</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mdsebook.robot.Event
		 * @see mdsebook.robot.impl.RobotPackageImpl#getEvent()
		 * @generated
		 */
		EEnum EVENT = eINSTANCE.getEvent();

	}

} //RobotPackage
