/**
 */
package mdsebook.robot;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mode Property</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see mdsebook.robot.RobotPackage#getModeProperty()
 * @model abstract="true"
 * @generated
 */
public interface ModeProperty extends EObject {
} // ModeProperty
