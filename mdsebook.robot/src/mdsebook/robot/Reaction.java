/**
 */
package mdsebook.robot;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reaction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mdsebook.robot.Reaction#getTarget <em>Target</em>}</li>
 *   <li>{@link mdsebook.robot.Reaction#getTrigger <em>Trigger</em>}</li>
 * </ul>
 *
 * @see mdsebook.robot.RobotPackage#getReaction()
 * @model
 * @generated
 */
public interface Reaction extends ModeProperty {
	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(Mode)
	 * @see mdsebook.robot.RobotPackage#getReaction_Target()
	 * @model
	 * @generated
	 */
	Mode getTarget();

	/**
	 * Sets the value of the '{@link mdsebook.robot.Reaction#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(Mode value);

	/**
	 * Returns the value of the '<em><b>Trigger</b></em>' attribute.
	 * The default value is <code>"EV_OBSTACLE"</code>.
	 * The literals are from the enumeration {@link mdsebook.robot.Event}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Trigger</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Trigger</em>' attribute.
	 * @see mdsebook.robot.Event
	 * @see #setTrigger(Event)
	 * @see mdsebook.robot.RobotPackage#getReaction_Trigger()
	 * @model default="EV_OBSTACLE" required="true"
	 * @generated
	 */
	Event getTrigger();

	/**
	 * Sets the value of the '{@link mdsebook.robot.Reaction#getTrigger <em>Trigger</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Trigger</em>' attribute.
	 * @see mdsebook.robot.Event
	 * @see #getTrigger()
	 * @generated
	 */
	void setTrigger(Event value);

} // Reaction
