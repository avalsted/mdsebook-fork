// (c) mddbook, wasowski, tberger
// Inspired by https://www.eclipse.org/forums/index.php/t/1075893/
package mdsebook.cmd

import scala.util.Try
import org.eclipse.emf.codegen.ecore.genmodel.GenModel
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.util.EcoreUtil

object Log { def log (s: String) = {
  println (s"mdsebook.cmd.GenerateModelCode: $s") }
  Console.flush()
}

object GenerateModelCode {

  import Log._

  def generate (genmodelPath: String, platformUri: String = "."): Unit = {

    (new org.eclipse.emf.mwe.utils.StandaloneSetup).setPlatformUri (platformUri)

    log ("Standalone setup completed.")

    // A magic incantation to register genmodel's meta-model
    // as suggested there: https://www.eclipse.org/forums/index.php/t/126755/
    org.eclipse.emf.codegen.ecore.genmodel.GenModelPackage.eINSTANCE.eClass

    log ("Ecore genmodel package registered.")

    val uri: URI = URI.createURI (genmodelPath)
    val resource: Resource = (new ResourceSetImpl).getResource (uri, true)

    val genmodel: GenModel =
      EcoreUtil.getAllProperContents[GenModel] (resource, false).next

    log (s"genmodel package loaded for ${genmodelPath}")

    val modelCodeDirectory =
      genmodel.getModelDirectory +"/"+
      genmodel.getGenPackages.get(0).getBasePackage +"/"+
      genmodel.getModelName.toLowerCase +"/"

    Try (new org.eclipse.emf.mwe.utils.DirectoryCleaner()
        .cleanFolder (s"${platformUri}/${modelCodeDirectory}/"))
    // If not there then already empty (ignore the exception)

    log ("Target directory cleaned")

    val generator = new org.eclipse.emf.mwe2.ecore.EcoreGenerator()
    generator.setGenerateCustomClasses (false)
    generator.setGenModel (genmodelPath)
    generator.invoke (null)

    log ("Code generated")
  }

}

object Main extends App {

  import Log._

  log ("Welcome!")

  args match {
    case Array(genmodel) =>
      GenerateModelCode.generate (genmodel)
    case Array(genmodel, platform) =>
      GenerateModelCode.generate (genmodel, platform)
    case _ =>
      println("""
              |Usage:   GenerateModelCode genmodel_uri
              |Example: ./GenerateModelCode platform:/resource/mdsebook.fsm/model/fsm.genmodel
              |
              |The invocation should be done from within an Eclipse Workspace,
              |probably in a root directory of a project in the same workspace
              |where the platform URI is referring to.
              |""".stripMargin)
  }

  log ("Bye, bye!")
}

