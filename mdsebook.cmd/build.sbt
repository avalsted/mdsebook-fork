// (c) mddbook, wasowski, tberger

name := "mdsebook.cmd"

libraryDependencies += "org.eclipse.emf" % "org.eclipse.emf.ecore" % "2.12.+"

libraryDependencies += "org.eclipse.emf" % "org.eclipse.emf.ecore.xmi" % "2.12.+"

libraryDependencies += "org.eclipse.emf" % "org.eclipse.emf.codegen.ecore" % "2.12.+"

libraryDependencies += "org.eclipse.emf" % "org.eclipse.emf.common" % "2.12.+"

libraryDependencies += "org.eclipse.emf" % "org.eclipse.emf.mwe.utils" % "1.3.13"

libraryDependencies += "org.eclipse.emf" % "org.eclipse.emf.mwe2.lib" %"2.8.3"

libraryDependencies += "com.google.guava" % "guava" % "26.0-jre"

libraryDependencies += "log4j" % "log4j" % "1.2.17"

