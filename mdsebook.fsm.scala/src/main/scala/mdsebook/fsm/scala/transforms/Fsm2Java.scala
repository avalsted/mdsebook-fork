// (c) mdsebook, wasowski, tberger
//
// Translate a a finite statem machine to a simple Java program.
// We use a minimal subset of Java, and little structure.  Almost the same
// code should be generated for C, to be used on a micro-controller (for example).
//
// The lack of control-string inversion like in template languages gets in the way
// here and there.

package mdsebook.fsm.scala.transforms

import scala.collection.JavaConversions._
import mdsebook.scala.EMFScala._
import mdsebook.fsm._
import java.io.File

import org.eclipse.emf.ecore.resource.Resource

object Fsm2Java 
{
  def fmtInputs (s: State): String =
    (s.getLeavingTransitions map { "<" + _.getInput + ">" }).mkString ("|    \"","","\"")

  def fmtState (state: State): String =
    s"""
        |          case ${state.getName.toUpperCase}:
        |            switch (input) {
                       ${(state.getLeavingTransitions map {
                            fmtTransition (_) }).mkString.dropRight(1)}|            }
        |            break;
        """

  def fmtTransition (t: Transition): String =
    s"""|              case "${t.getInput}":
        |                System.out.println ("machine says: ${t.getOutput}");
        |                current = ${t.getTarget.getName.toUpperCase};
        |                break;
        """

  def compileToJava(it: FiniteStateMachine): String = {

    var i: Int = -1

    List (s"""|import java.util.Scanner;
              |
              |class FSM${it.getName.capitalize} {
              |
              """,
                 (it.getStates map { (s :State) =>
          s"""|  static final int ${s.getName.toUpperCase} = ${i = i+1; i};
          """ }) .mkString,
          s"""|
              |  static int current;

              |  static final String[] stateNames = {
              |    ${it.getStates map {'"' + _.getName + '"'} mkString (", ") }
              |  };
              |
              |  static final String[] availableInputs = {
                   ${it.getStates map { fmtInputs (_) } mkString (",\n") }
              |  };
              |
              |  public static void main (String[] args) {
              |
              |    @SuppressWarnings(value = { "resource" })
              |    Scanner scanner = new Scanner (System.in);
              |    current = ${it.getInitial.getName.toUpperCase};
              |
              |    while (true) {
              |
              |      System.out.print ("[" + stateNames[current] + "] ");
              |      System.out.print ("What is the next event? available: "
              |                           + availableInputs[current]);
              |      System.out.print ("?");
              |      String input = scanner.nextLine();
              |
              |      switch (current) {
                       ${(it.getStates map { fmtState (_) }).mkString}
              |      }
              |    }
              |  }
              |}
              |""").mkString.stripMargin
  }

}