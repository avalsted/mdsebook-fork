// (c) mdsebook, wasowski, berger
// Run FSM interpreter with a textual frontend.
// for loading and parsing outside Eclipse.

package mdsebook.fsm.scala.external

import scala.collection.JavaConversions._

import mdsebook.fsm.FsmPackage
import mdsebook.fsm.FiniteStateMachine
import mdsebook.fsm.Model
import mdsebook.fsm.scala.Interpreter
import mdsebook.scala.EMFScala._
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl

object Main extends App {

	// Change input file name of your state machine model here;
	// path is relative to project root
	val instanceFileName = "../mdsebook.fsm/test-files/CoffeeMachine.xmi"

  // register a resource factory for XMI files
  Resource.Factory.Registry.INSTANCE.
  getExtensionToFactoryMap.put("xmi", new XMIResourceFactoryImpl)

  // Register our meta-model package for abstract syntax
  FsmPackage.eINSTANCE.eClass

  // we are loading our file here
  val uri = URI.createURI (instanceFileName)
  var resource = new ResourceSetImpl().getResource (uri, true);

  // The call to get(0) below gives you the first model root.
  // If you open a directory instead of a file,
  // you can iterate over all models in it,
  // by changing 0 to a suitable index
  val model = resource.getContents.get (0).asInstanceOf[Model]

  // We are reusing the interpreter from internalDSL.
  // It would be more elegant to refactor this,
  // moving the interpreter to the general fsm package
  // (as it is independent of the syntax technology)

  Interpreter.step (model.getMachines.get(0).getInitial)
}

