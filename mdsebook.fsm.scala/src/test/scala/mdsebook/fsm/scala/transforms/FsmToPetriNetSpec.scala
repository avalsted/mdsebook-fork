// (c) mddbook, wasowski, tberger

package mdsebook.fsm.scala.transforms

import scala.collection.JavaConversions._

import org.eclipse.emf.common.util.Diagnostic
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.util.Diagnostician
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl
import org.scalatest.FlatSpec
import org.scalatest.Matchers
import org.junit.runner.RunWith

import mdsebook.scala.EMFScala

import mdsebook.fsm
import mdsebook.{ petrinet => PN }

@RunWith(classOf[org.scalatest.junit.JUnitRunner])
class FsmToPetriNetSpec extends FlatSpec with Matchers {

  // Initialize EMF

  Resource.Factory.Registry.INSTANCE.
    getExtensionToFactoryMap.put("xmi", new XMIResourceFactoryImpl)
  fsm.FsmPackage.eINSTANCE.eClass
	PN.PetrinetPackage.eINSTANCE.eClass

  // Set up fixtures. Note: imperative tests destroy fixtures

  val ids = List ("test-00", "test-01", "test-02", "test-03", "test-04", "test-05", "test-06", "test-07")
  val N = ids.size-1
  val fnames = ids map (t => s"../mdsebook.fsm/test-files/$t.xmi")
  val roots :List[fsm.Model] = fnames map {EMFScala loadFromXMI[fsm.Model] _}

  sealed case class Fixture (root :fsm.Model, id :String)

  def fixture (n: Int) = Fixture (
      root = EcoreUtil.copy(roots(n)).asInstanceOf[fsm.Model], // yuck
      id = ids(n) )

  // helper methods

  def validates (root: PN.Model): Boolean =
    Diagnostician.INSTANCE.validate (root).getSeverity == Diagnostic.OK

  def validates (root: fsm.Model): Boolean =
    Diagnostician.INSTANCE.validate (root).getSeverity == Diagnostic.OK

  // tests

  behavior of "FsmToPetriNet"

  it should s"give results that validate in the new metamodel" in {

    val in = 0 to N map (fixture _) filter { m => validates (m.root) }
    val out = in map { FsmToPetriNet run  _.root }
    out foreach { o => validates (o) shouldBe true }
  }

  it should s"save results of each transformations into new pn xmi format" in {

    val in = 0 to N map (fixture _) filter { m => validates (m.root) }
    val out = in.map { f => f.id -> (FsmToPetriNet run  f.root) }
    out foreach { m => (EMFScala.saveAsXMI (s"test-out/${m._1}.pn.xmi") (m._2)) }

    // let's see whether they load without errors

    val reloaded = out map { m => EMFScala.loadFromXMI[PN.Model](s"test-out/${m._1}.pn.xmi") }
    reloaded foreach { m => validates (m) shouldBe true }

  }

}
