/**
 */
package mdsebook.fsm2;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mdsebook.fsm2.Model#getMachines <em>Machines</em>}</li>
 * </ul>
 *
 * @see mdsebook.fsm2.Fsm2Package#getModel()
 * @model
 * @generated
 */
public interface Model extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Machines</b></em>' containment reference list.
	 * The list contents are of type {@link mdsebook.fsm2.FiniteStateMachine}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machines</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machines</em>' containment reference list.
	 * @see mdsebook.fsm2.Fsm2Package#getModel_Machines()
	 * @model containment="true"
	 * @generated
	 */
	EList<FiniteStateMachine> getMachines();

} // Model
