/**
 */
package mdsebook.fsm2;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Finite State Machine</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mdsebook.fsm2.FiniteStateMachine#getStates <em>States</em>}</li>
 * </ul>
 *
 * @see mdsebook.fsm2.Fsm2Package#getFiniteStateMachine()
 * @model
 * @generated
 */
public interface FiniteStateMachine extends NamedElement {
	/**
	 * Returns the value of the '<em><b>States</b></em>' containment reference list.
	 * The list contents are of type {@link mdsebook.fsm2.State}.
	 * It is bidirectional and its opposite is '{@link mdsebook.fsm2.State#getMachine <em>Machine</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>States</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>States</em>' containment reference list.
	 * @see mdsebook.fsm2.Fsm2Package#getFiniteStateMachine_States()
	 * @see mdsebook.fsm2.State#getMachine
	 * @model opposite="machine" containment="true" required="true"
	 * @generated
	 */
	EList<State> getStates();

} // FiniteStateMachine
